<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class a_user_logs_in_Test extends TestCase
{
    public function testExample()
    {
        User::where('username','testlogin')->delete();
        $user =  factory(User::class)->create(
          ['username' => 'testlogin',
           'password' => bcrypt('testlogin'),
           'role' => '1'
          ]);

        $this->visit('/');
        $this->type($user->email, 'username');
        $this->type($user->password, 'password');
        $this->press('Login');
        $this->assertTrue(Auth::check());
        $this->seePageIs(route('/home'));
    }
}
