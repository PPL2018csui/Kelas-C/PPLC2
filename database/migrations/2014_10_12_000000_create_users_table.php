<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('name');
            $table->string('email')->unique();
            $table->integer('role');
            $table->unsignedInteger('faculty_id')->nullable()->default(null);
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('faculty_id')
            ->references('id')->on('faculties')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
      Schema::dropIfExist('users');
      DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
