<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusBerkasFakultasStatusBerkasSdmToCalonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('calons', function($table)
      {
          $table->string('statusBerkasFakultas')->default('0000000000000000');
          $table->string('statusBerkasSdm')->default('0000000000000000');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('calons', function($table)
      {
          $table->dropColumn('statusBerkasFakultas');
          $table->dropColumn('statusBerkasSdm');
      });
    }
}
