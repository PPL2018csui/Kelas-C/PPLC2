<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('faculties')->delete();

      $faculties = [
        ['name' => 'Fakultas Ilmu Komputer'],
        ['name' => 'Fakultas Kedokteran'],
        ['name' => 'Fakultas Ekonomi dan Bisnis'],
        ['name' => 'Fakultas Hukum'],
        ['name' => 'Fakultas Teknik'],
        ['name' => 'Fakultas Farmasi'],
        ['name' => 'Fakultas Psikologi'],
        ['name' => 'Fakultas Kedokteran Gigi'],
        ['name' => 'Fakultas Ilmu Keperawatan'],
        ['name' => 'Fakultas Kesehatan Masyarakat'],
        ['name' => 'Fakultas Ilmu Budaya'],
        ['name' => 'Fakultas Ilmu Sosial dan Ilmu Politik'],
        ['name' => 'Fakultas Matematika dan Ilmu Pengetahuan Alam']
      ];

      DB::table('faculties')->insert($faculties);
      DB::table('users')->insert([
        'name' => 'Admin',
        'username' => 'admin',
        'email' => 'admin@ui.ac.id',
        'role' => 1,
        'password' => Hash::make('admin123')
      ]);

      DB::table('users')->insert([
        'name' => 'Fakultas',
        'username' => 'fakultas',
        'email' => 'fakultas@cs.ui.ac.id',
        'role' => 2,
        'faculty_id' => 1,
        'password' => Hash::make('fakultas123')
      ]);

      DB::table('users')->insert([
        'name' => 'Sdm',
        'username' => 'sdm',
        'email' => 'sdm@sdm.ui',
        'role' => 3,
        'password' => Hash::make('sdm123')
      ]);

      DB::table('users')->insert([
        'name' => 'Dgb',
        'username' => 'dgb',
        'email' => 'dgb@dgb.ui',
        'role' => 4,
        'password' => Hash::make('dgb123')
      ]);

      $calons = [
        ['name'=> 'abcd', 'nip' => 214354, 'faculty_id'=> 1, 'usulan' => 1, 'user_id' => 1],
        ['name'=> 'efgh', 'nip' => 214355, 'faculty_id'=> 1, 'usulan' => 2, 'user_id' => 1],
        ['name'=> 'ijkl', 'nip' => 214356, 'faculty_id'=> 2, 'usulan' => 2, 'user_id' => 1],
        ['name'=> 'mnop', 'nip' => 214357, 'faculty_id'=> 2, 'usulan' => 3, 'user_id' => 1],
        ['name'=> 'qrst', 'nip' => 214358, 'faculty_id'=> 4, 'usulan' => 4, 'user_id' => 1],
        ['name'=> 'uvwx', 'nip' => 214359, 'faculty_id'=> 4, 'usulan' => 5, 'user_id' => 1]
      ];

      DB::table('calons')->insert($calons);


    }
}
