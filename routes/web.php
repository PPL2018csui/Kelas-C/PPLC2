<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
})->middleware('guest');

Auth::routes();

Route::get('/home', function () {
    return view('homepage');
})->middleware('auth');

Route::get('/migrate',function(){
    Artisan::call('migrate:refresh');
});

Route::get('/seed',function(){
    Artisan::call('db:seed');
});


Route::prefix('managefaculty')->group(function () {
    Route::get('/', 'ManageFacultyController@index');
    Route::get('/edit/{id}', 'ManageFacultyController@editView');
    Route::post('/edit', 'ManageFacultyController@editData');
    Route::get('/create', 'ManageFacultyController@createView');
    Route::post('/create', 'ManageFacultyController@createNew');
    Route::get('/delete/{id}/', 'ManageFacultyController@deleteFaculty');
});

Route::prefix('calon')->group(function(){
    Route::get('/','CalonController@index');
    Route::get('/faculty/{facultyName}','CalonController@singleFaculty');
    Route::get('/create','CalonController@create');
    Route::post('/create','CalonController@createData');
    Route::get('/detail/{nip}','CalonController@singleCalon');
    Route::get('/edit/{nip}', 'CalonController@editView');
    Route::post('/edit', 'CalonController@editData');
    Route::get('/delete/{nip}', 'CalonController@deleteCalon');
    Route::get('/verifikasi/{nip}','CalonController@verifyCalonView');

});

Route::get('/download/{nip}/{id}','CalonController@download');

Route::get('/form/{nip}','FormController@index');
// Route::prefix('form')->group(function(){
//     Route::get('/1',function(){return view('form/form1');});
//     Route::get('/2',function(){return view('form/form2');});
//     Route::get('/3',function(){return view('form/form3');});
//     Route::get('/4',function(){return view('form/form4');});
//     Route::get('/5',function(){return view('form/form5');});
// });

Route::get('/form/fill/{nip}','FillFormController@index');
