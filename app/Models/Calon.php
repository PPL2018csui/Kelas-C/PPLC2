<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Calon extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'name', 'nip', 'usulan','user_id','faculty_id',
  ];

  public function faculty() {
    return $this->belongsTo('App\Models\Faculty');
  }
}
