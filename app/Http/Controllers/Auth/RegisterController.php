<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Models\Faculty;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard();
        $message = 'User baru berhasil dibuat.';
        return $this->registered($request, $user)?
                  : redirect($this->redirectPath())->with('message', $message);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'username' => 'required|max:25|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'role' => 'required|integer',
            'faculty_id' => 'integer',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if ($data['role'] == 2) {
          return User::create([
              'name' => $data['name'],
              'username' => $data['username'],
              'email' => $data['email'],
              'password' => Hash::make($data['password']),
              'role' => $data['role'],
              'faculty_id' => $data['faculty'],
          ]);
        } else {
          return User::create([
              'name' => $data['name'],
              'username' => $data['username'],
              'email' => $data['email'],
              'password' => Hash::make($data['password']),
              'role' => $data['role'],
          ]);
        }

    }

    public function showRegistrationForm()
    {
      $faculties = Faculty::all();
      return view('auth.register', compact('faculties'));
    }
}
