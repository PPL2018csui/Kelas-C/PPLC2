<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Faculty;
use Validator;

class ManageFacultyController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $faculties = Faculty::orderBy('id')->get();
        return view('manageFaculty.index', compact('faculties'));
    }

    public function createView()
    {
        $faculties = Faculty::all();
        return view('manageFaculty.create',compact('faculties'));
    }

    public function createNew(Request $req)
    {
        $validator = Validator::make($req->all(), [
            'name'   => 'required|max:50|unique:faculties',
        ]);

        if ($validator->fails()) {
            $error = explode(" ",$validator->errors()->first());
            if ($error[2] == "has") {
              $message = "Nama fakultas sudah terdaftar";
              return redirect()->back()->withErrors($message);
            }else {
              $message = "Nama fakultas tidak boleh lebih dari 50 karakter";
              return redirect()->back()->withErrors($message);
            }
        }

        $faculty = new Faculty;
        $faculty->name = $req->name;
        $faculty->save();
        $message = $req->name .' berhasil ditambahkan.';
        return redirect('/managefaculty')->with('message',  $message);
    }

    public function editView($id)
    {
        $faculty = Faculty::find($id);
        return view('manageFaculty.edit',compact('faculty'));
    }

    public function editData(Request $req){
      $faculty = Faculty::find($req->id);
      $oldname = $faculty->name;
      $faculty->name = $req->name;
      $faculty->save();

      $message = $oldname . " berhasil diganti menjadi " . $req->name;
      return redirect('/managefaculty')->with('message', $message);
    }

    public function deleteFaculty($id)
    {
        $facultyName = Faculty::find($id)->name;
        Faculty::where('id', $id)->delete();
        $message = $facultyName . ' berhasil dihapus.';
        return redirect()->back()->with('message', $message);
    }
}
