<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Calon;
use App\Models\Faculty;

class FillFormController extends Controller
{
  public function index($nip){
    $calon = Calon::where('nip', $nip)->first();
    $faculty = Faculty::where('id', $calon->faculty_id)->first();
    $facultyName = $faculty->name;
    return view('isiForm/isiForm'.$calon->usulan , compact('calon', 'faculty', 'facultyName'));
  }
}
