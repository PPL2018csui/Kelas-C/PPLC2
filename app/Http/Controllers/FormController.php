<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Models\Calon;
use App\Models\Faculty;

class FormController extends Controller
{
  public function index($nip){
    $calon = Calon::where('nip', $nip)->first();
    $faculty = Faculty::where('id', $calon->faculty_id)->first();
    $facultyName = $faculty->name;
    $pdf = PDF::loadView('form.form' . $calon->usulan, ['calon' => $calon, 'facultyName' => $facultyName]);
    return $pdf->stream('form' . $calon->usulan . '.pdf');
  }
}
