<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Faculty;
use App\Models\Calon;
use Auth;
use Validator;
use Illuminate\Support\Facades\Storage;

class CalonController extends Controller
{
  public function __construct()
  {

  }

  public function index()
  {
    if (Auth::user()->role == 2) {
      $faculty = Faculty::find(Auth::user()->faculty_id);
      $url = "/calon/faculty/" . str_replace(" ", "-", $faculty->name);
      return redirect($url);
    }
    $faculties = Faculty::orderBy('id')->get();
    return view('calon.listFaculty', compact('faculties'));
  }

  public function create()
  {
    if (Auth::user()->role == 1) {
      $faculties = Faculty::all();
      return view('calon.create', compact('faculties'));
    }
    else if (Auth::user()->role == 2) {
      $faculty = Faculty::find(Auth::user()->faculty_id);
      return view('calon.create', compact('faculty'));
    }

  }

  public function createData(Request $req){
    if (!empty($req->file)) {
        for ($i=0; $i < 16; $i++) {
          Storage::makeDirectory('public/dokumenCalon/' . $req->nip . '/' . $i);
          if (!empty($req->file[$i])) {
              $filename = $req->file[$i]->storeAs('public/dokumenCalon/' . $req->nip . '/' . $i . '/' , $req->file[$i]->getClientOriginalName());
          }
        }
    }
    $validator = Validator::make($req->all(), [
        'name'       => 'required|max:50',
        'nip'        => 'required|integer',
        'faculty_id' => 'required|integer',
        'usulan'     => 'required|integer',
    ]);

    if ($validator->fails()) {
        return redirect()->back()->withErrors($validator);
    }

    $calon = new Calon;
    $calon->name = $req->name;
    $calon->nip = $req->nip;
    $calon->faculty_id = $req->faculty_id;
    $calon->usulan = $req->usulan;
    $calon->user_id = Auth::user()->id;
    $calon->save();

    return redirect()->back()->with('message', 'Calon berhasil ditambahkan!');
  }

  public function singleFaculty($facultyName){
    $name = str_replace('-', ' ', $facultyName);
    $faculty = Faculty::where('name', $name)->first();
    $calons = Calon::where('faculty_id', $faculty->id)->get();
    return view('calon.singleFaculty', compact('calons','facultyName'));
  }

  public function singleCalon($nip){
    $calon = Calon::where('nip', $nip)->first();
    $faculty = Faculty::find($calon->faculty_id);
    $facultyName = $faculty->name;
    return view('calon.singleCalon', compact('calon', 'facultyName'));
  }

  public function editView($nip)
  {
    if (Auth::user()->role == 1 || Auth::user()->role == 2) {
      $faculties = Faculty::all();
      $calon = Calon::where('nip', $nip)->first();
      $faculty = Faculty::find($calon->faculty_id);
      $facultyName = $faculty->name;
      return view('calon.edit',compact('calon', 'facultyName', 'faculties'));
    }

  }

  public function editData(Request $req){
    $validator = Validator::make($req->all(), [
        'name'       => 'required|max:50',
        'nip'        => 'required|integer',
        'faculty_id' => 'required|integer',
        'usulan'     => 'required|integer',
    ]);

    if ($validator->fails()) {
        return redirect()->back()->withErrors($validator);
    }

    $calon = Calon::where('nip', $req->nip)->first();
    $calon->name = $req->name;
    $calon->nip = $req->nip;
    $calon->faculty_id = $req->faculty_id;
    $calon->usulan = $req->usulan;
    $calon->save();
    $message = 'Data calon berhasil diubah.';
    return redirect('/calon/detail/' . $calon->nip)->with('message', $message);
  }

  public function deleteCalon($nip)
  {
      $calon = Calon::where('nip', $nip)->first();
      $calonName = $calon->name;
      $faculty = Faculty::find($calon->faculty_id);
      $facultyName = str_replace(' ', '-', $faculty->name);
      Calon::where('nip', $nip)->delete();
      $message = 'Calon ' . $calonName . ' successfully deleted!';
      return redirect('/calon/faculty/' . $facultyName)->with('message', $message);
  }

  public function verifyCalonView($nip)
  {
    $calon = Calon::where('nip', $nip)->first();
    $faculty = Faculty::find($calon->faculty_id);
    for ($i=0; $i < 16; $i++) {
      $folder[$i] = empty(Storage::allFiles('public/dokumenCalon/'.$nip.'/' . $i));
    }
    return view('calon.verifikasi', compact('calon','faculty', 'folder'));
  }

  public function download($nip, $id){
    $file = Storage::allFiles('public/dokumenCalon/'.$nip.'/' . $id);
    return Storage::download($file[0]);
  }

}
