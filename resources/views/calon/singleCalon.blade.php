@extends('layouts.app')

@section('content')
  @if(session()->has('message'))
      <div id="id01" class="w3-modal" style="display:block">
          <div class="w3-modal-content w3-card-4 w3-green w3-round-large" style="width:300px;height:400px;text-align:center">
              <i class="fas fa-check" style="font-size:150px;"></i>
              <br>
              <h1>SUCCESS</h1>
              <br>
              <p class="w3-padding">{{ session()->get('message') }}</p>
              <button onclick="document.getElementById('id01').style.display='none'"
                class="w3-button w3-margin-bottom w3-hover-black w3-light-grey w3-text-grey w3-hover-opacity w3-border w3-round-large c2-ssp-regular w3-display-bottommiddle"
                style="width:80%">
                Ok
              </button>
          </div>
      </div>
  @endif
<div class="w3-container w3-padding-large">
  <br>
  <h2 class="c2-domine w3-center">Lihat Data Calon</h2>
  <br>
  <div class="w3-border w3-round-large w3-light-grey" style="max-width:400px;margin:0 auto">
    <div class="c2-ssp-regular"style="margin:10px;">
      <table>
        <tr>
          <td>Nama Lengkap</td>
          <td>: {{ $calon->name }}</td>
        </tr>
        <tr>
          <td>NIP/NUP</td>
          <td>: {{ $calon->nip }}</td>
        </tr>
        <tr>
          <td>Unit Kerja/Fakultas</td>
          <td>: {{$facultyName}}</td>
        </tr>
        <tr>
          <td>Usulan</td>
          <td>:
            @if($calon->usulan == 1)
              Lektor Kepala ke Guru Besar
            @elseif($calon->usulan == 2)
              Lektor ke Guru Besar
            @elseif($calon->usulan == 3)
              Lektor ke Lektor Kepala
            @elseif($calon->usulan == 4)
              Asisten Ahli ke Lektor Kepala
            @elseif($calon->usulan == 5)
              Jabatan yang sama
            @endif
          </td>
        </tr>
        <tr>
          <td>Status</td>
          <td>: Tidak Diterima</td>
        </tr>
      </table>
      <br>
      <div class="w3-center">
          <a href="#{{--/calon/verifikasi/{{$calon->nip}}--}}" class="w3-btn w3-border w3-hover-yellow w3-round-large c2-background-grey w3-text-white" name="button">Lihat berkas</a>
          <a href="#{{--/form/fill/{{$calon->nip}}--}}" class="w3-btn w3-border w3-hover-yellow w3-round-large c2-background-grey w3-text-white">Isi Form Penilaian</a>
          <a href="/form/{{$calon->nip}}" class="w3-btn w3-border w3-hover-yellow w3-round-large c2-background-grey w3-text-white" target="_blank">Unduh Form</a>
          <a href="/calon/edit/{{$calon->nip}}" class="w3-btn w3-border w3-hover-yellow w3-round-large c2-background-grey w3-text-white">Edit Calon</a>
          <button onclick="document.getElementById('{{$calon->nip}}').style.display='block'"
            class="w3-btn w3-border w3-hover-yellow w3-round-large c2-background-grey w3-text-white">Delete Calon</button>
          <div id="{{$calon->nip}}" class="w3-modal w3-animate-opacity">
              <div class="w3-modal-content w3-card-4">
                  <header class="w3-container c2-background-yellow">
                      <h3>Confirmation</h3>
                  </header>
                  <div class="w3-container">
                      <p>Apakah anda yakin untuk menghapus {{$calon->name}}?</p>
                  </div>
                  <footer class="w3-container c2-background-yellow w3-padding">
                      <span onclick="document.getElementById('{{$calon->nip}}').style.display='none'"
                        class="w3-button w3-right w3-hover-black c2-background-grey w3-text-white w3-round-xlarge c2-ssp-regular">Cancel</span>
                          <form action="/calon/delete/{{$calon->nip}}/" method="get">
                            <button class="w3-button w3-right w3-hover-black w3-margin-right c2-background-grey w3-text-white w3-round-xlarge c2-ssp-regular">Delete</button>
                          </form>
                  </footer>
              </div>
          </div>
      </div>
      <br>
    </div>
  </div>

</div>

@endsection
