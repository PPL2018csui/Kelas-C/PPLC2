@extends('layouts.app')

@section('content')
  @if(session()->has('message'))
    <div id="id01" class="w3-modal" style="display:block">
        <div class="w3-modal-content w3-card-4 w3-green w3-round-large" style="width:300px;height:400px;text-align:center">
            <i class="fas fa-check" style="font-size:150px;"></i>
            <br>
            <h1>SUCCESS</h1>
            <br>
            <p>Calon baru berhasil ditambahkan</p>
            <button onclick="document.getElementById('id01').style.display='none'"
              class="w3-button w3-margin-bottom w3-hover-black w3-light-grey w3-text-grey w3-hover-opacity w3-border w3-round-large c2-ssp-regular w3-display-bottommiddle"
              style="width:80%">
              Ok
            </button>
        </div>
    </div>
  @endif
<h2 class="c2-text-grey c2-domine" style="text-align:center;margin-top:40px">Tambah Calon</h2>
<form class="w3-container" method="POST" action="/calon/create" enctype="multipart/form-data">
<div class="w3-container" style="max-width:400px;padding:10px;margin:auto;margin-top:30px">
    <div class="w3-round-large c2-background-grey c2-ssp-regular w3-padding-16">

          <div class="w3-padding">
            <input id="namalengkap" class="w3-text-white w3-border-bottom c2-background-grey w3-input w3-border-yellow c2-width-100"  type="text" placeholder="Nama Lengkap Dosen" name="name" required></input>
          </div>
          <div class="w3-padding">
            <input id="nip-dosen" class="w3-text-white w3-border-bottom c2-background-grey w3-input w3-border-yellow c2-width-100" style="margin-bottom:10px" type="text" placeholder="NIP/NUP" name="nip" required></input>
          </div>
          <div class="w3-padding">
            <select id="faculty_id" class="w3-text-light-grey w3-border-bottom c2-background-grey w3-input w3-border-yellow c2-width-100" style="margin-bottom:10px" name="faculty_id" required>
                <option value="" disabled selected>Unit Kerja/Fakultas</option>
                @if (Auth::user()->role == 2)
                  <option value="{{$faculty->id}}">{{$faculty->name}}</option>
                @else
                  @foreach ($faculties as $faculty)
                    <option value="{{$faculty->id}}">{{$faculty->name}}</option>
                  @endforeach
                @endif
            </select>
          </div>
          <div class="w3-padding">
            <select id="usulan" class="w3-text-light-grey w3-border-bottom c2-background-grey w3-input w3-border-yellow c2-width-100" style="margin-bottom:10px" name="usulan" required>
                <option value="" disabled selected>Usulan</option>
                <option value="1">Lektor Kepala ke Guru Besar</option>
                <option value="2">Lektor ke Guru Besar</option>
                <option value="3">Lektor ke Lektor Kepala</option>
                <option value="4">Asisten Ahli ke Lektor Kepala</option>
                <option value="5">Jabatan yang sama</option>
            </select>
          </div>/
    </div>
</div>

<div class="w3-container" style="max-width:1200px;padding:10px;margin:auto;margin-top:50px">
    <div class="w3-round-large w3-padding-16">
        <table class="w3-table-all c2-ssp-regular c2-background-grey w3-hoverable w3-center">

            <tr class="w3-light-grey">
                <th class="w3-center c2-ssp-regular c2-background-grey w3-text-white">No.</th>
                <th class="w3-center c2-ssp-regular c2-background-grey w3-text-white">Isi Berkas</th>
                <th class="w3-center c2-ssp-regular c2-background-grey w3-text-white">Format Berkas</th>
                <th class="w3-center c2-ssp-regular c2-background-grey w3-text-white">Upload Berkas</th>
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">1</td>
                <td>Scan ijazah terakhir yang disahkan oleh pejabat yang berwenang</td>
                <td class="w3-center">Pdf</td>
                <td class="w3-center"><input type="file" id="file1" name="file[]"></td>
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">2</td>
                <td>Scan ijazah luar negeri serta SK penyetaraan dari Ditjen DIKTI (jika lulusan luar negeri)</td>
                <td class="w3-center">Pdf</td>
                <td class="w3-center"><input type="file" id="file2" name="file[]"></td>
            </tr>


            <tr class="c2-background-grey">
              <td class="w3-center">3</td>
              <td>Scan abstrak Disertasi/ Thesis</td>
              <td class="w3-center">Pdf</td>
              <td class="w3-center"><input type="file" id="file3" name="file[]"></td>
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">4</td>
                <td>Scan surat keputusan pemberian tugas belajar, disahkan oleh pejabat yang berwenang (jika ada)</td>
                <td class="w3-center">Pdf</td>
                <td class="w3-center"><input type="file" id="file4" name="file[]"></td>
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">5</td>
                <td>Scan surat keputusan pengaktifan kembali setelah selesai melaksanakan tugas belajar, disahkan oleh pejabat berwenang (jika ada)</td>
                <td class="w3-center">Pdf</td>
                <td class="w3-center"><input type="file" id="file5" name="file[]"></td>
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">6</td>
                <td>Scan DUPAK yang telah ditandatangani oleh pejabat berwenang</td>
                <td class="w3-center">Pdf</td>
                <td class="w3-center"><input type="file" id="file6" name="file[]"></td>
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">7</td>
                <td>Scan PAK terakhir, disahkan oleh pejabat yang berwenang</td>
                <td class="w3-center">Pdf</td>
                <td class="w3-center"><input type="file" id="file7" name="file[]"></td>
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">8</td>
                <td>Scan surat keputusan jabatan terakhir, disahkan oleh pejabat yang berwenang</td>
                <td class="w3-center">Pdf</td>
                <td class="w3-center"><input type="file" id="file8" name="file[]"></td>
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">9</td>
                <td>Scan surat keputusan kenaikan pangkat terakhir, disahkan oleh pejabat yang berwenang</td>
                <td class="w3-center">Pdf</td>
                <td class="w3-center"><input type="file" id="file9" name="file[]"></td>
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">10</td>
                <td>Scan PPKP (Penilaian Prestasi Kerja Pegawai) dua tahun terakhir, disahkan pejabat berwenang</td>
                <td class="w3-center">Pdf</td>
                <td class="w3-center"><input type="file" id="file10" name="file[]"></td>
            </tr>

                <tr class="c2-background-grey">
                <td class="w3-center">11</td>
                <td>Scan Surat pernyataan melaksanakan penelitian</td>
                <td class="w3-center">Pdf</td>
                <td class="w3-center"><input type="file" id="file1" name="file[]"></td>
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">12</td>
                <td>Scan surat pernyataan Pengesahan Hasil Validasi Karya Ilmiah</td>
                <td class="w3-center">Pdf</td>
                <td class="w3-center"><input type="file" id="file12" name="file[]"></td>
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">13</td>
                <td>Berkas berisi tautan karya ilmiah (lihat lampiran 4, surat edaran 1864/2015 Dirjen Dikti)</td>
                <td class="w3-center">Ms Word</td>
                <td class="w3-center"><input type="file" id="file13" name="file[]"></td>
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">14</td>
                <td>Berkas berisi hasil similarity checking untuk setiap karya ilmiah, di satukan dalam format zip. Setiap hasil similarity checking memuat nomor sesuai urutan karya ilmiah (lihat Surat Edaran Dirjen SDIPT KemenristekDikti ) No 1753/2016</td>
                <td class="w3-center">zip</td>
                <td class="w3-center"><input type="file" id="file14" name="file[]"></td>
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">15</td>
                <td>Scan Sertifikat Pendidik untuk Dosen</td>
                <td class="w3-center">Pdf</td>
                <td class="w3-center"><input type="file" id="file15" name="file[]"></td>
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">16</td>
                <td>Scan setiap lembar peer review karya ilmiah ( berikan nama berkas sesuai daftar usulan dan simpan dalam satu folder)</td>
                <td class="w3-center">Pdf</td>
                <td class="w3-center"><input type="file" id="file16" name="file[]"></td>
              </tr>

        </table>
    </div>
    <div class="w3-center">
        {{csrf_field()}}
        <button type="submit" name="button" class="w3-button w3-hover-black c2-background-grey w3-text-white w3-border w3-round-xlarge c2-ssp-regular">Submit</button>
    </div>

    <div class="w3-left">
        <p class="c2-text-grey c2-ssp-regular">Catatan: Seluruh berkas di atas, disimpan ke dalam CD dan dikirimkan ke Rektorat UP Direktorat SDM-UI, setelah diperiksa dikirim ke sekretariat DGB-UI</p>
    </div>
</div>
</form>

@endsection
