@extends('layouts.app')

@section('content')

<div class="w3-container w3-padding-large">
  <br>
  <h2 class="c2-domine w3-center">Lihat List Calon</h2>
  <br>
  <br>

    @foreach ($faculties as $faculty)
    <div class="w3-col l4 m12 w3-padding c2-ssp-regular">
      <div style="">
        <a href="/calon/faculty/{{str_replace(' ', '-', $faculty->name)}}" style="text-decoration:none;">
        <div class="w3-panel w3-card-2 w3-white w3-round-large w3-margin" style="padding:10px;">
          <img class="w3-image responsive w3-left" src="{{asset('assets/img/fakultas.png')}}" style="width:10%">
          <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$faculty->name}}</p>
        </div>
        </a>
      </div>
    </div>
    @endforeach

</div>

@endsection
