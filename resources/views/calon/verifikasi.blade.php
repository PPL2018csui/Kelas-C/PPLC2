@extends('layouts.app')

@section('content')

<h2 class="c2-text-grey c2-domine" style="text-align:center;margin-top:40px">Verifikasi Calon</h2>
<div class="w3-container" style="max-width:1200px;padding:10px;margin:auto;margin-top:50px">
  <div class="w3-container" style="max-width:400px;padding:10px;margin:auto;margin-top:30px">
      <div class="w3-round-large c2-background-grey c2-ssp-regular w3-padding-16">

            <div class="w3-padding">
              <div id="namalengkap" class="w3-text-white w3-border-bottom c2-background-grey w3-input w3-border-yellow c2-width-100">{{$calon->name}}</div>
            </div>
            <div class="w3-padding">
              <div id="nip-dosen" class="w3-text-white w3-border-bottom c2-background-grey w3-input w3-border-yellow c2-width-100" >{{$calon->nip}}</div>
            </div>
            <div class="w3-padding">
              <div id="nip-dosen" class="w3-text-white w3-border-bottom c2-background-grey w3-input w3-border-yellow c2-width-100" >{{$faculty->name}}</div>
            </div>
            <div class="w3-padding">
              <div id="nip-dosen" class="w3-text-white w3-border-bottom c2-background-grey w3-input w3-border-yellow c2-width-100" >
                @if($calon->usulan == 1)
                  Lektor Kepala ke Guru Besar
                @elseif($calon->usulan == 2)
                  Lektor ke Guru Besar
                @elseif($calon->usulan == 3)
                  Lektor ke Lektor Kepala
                @elseif($calon->usulan == 4)
                  Asisten Ahli ke Lektor Kepala
                @elseif($calon->usulan == 5)
                  Jabatan yang sama
                @endif
              </div>
            </div>/
      </div>
  </div>

    <div class="w3-round-large w3-padding-16">
        <table class="w3-table-all c2-ssp-regular c2-background-grey w3-hoverable w3-center">

            <tr class="w3-light-grey">
                <th class="w3-center c2-ssp-regular c2-background-grey w3-text-white">No.</th>
                <th class="w3-center c2-ssp-regular c2-background-grey w3-text-white">Isi Berkas</th>
                <th class="w3-center c2-ssp-regular c2-background-grey w3-text-white">Download Berkas</th>
                <th class="w3-center c2-ssp-regular c2-background-grey w3-text-white">Diperiksa SDM-UI</th>
                <th class="w3-center c2-ssp-regular c2-background-grey w3-text-white">Diperiksa SDM-Fakultas</th>
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">1</td>
                <td>Scan ijazah terakhir yang disahkan oleh pejabat yang berwenang</td>
                @if (($folder[0]))
                  <td class="w3-center"><a href="#" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow" style="pointer-events: none;">Berkas belum diunggah</a></td>
                @else
                  <td class="w3-center"><a href="/download/{{$calon->nip}}/0" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow">Unduh</a></td>
                @endif
                @if ($calon->statusBerkasSdm{0} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxSdm[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxSdm[]"></td>
                @endif
                @if ($calon->statusBerkasFakultas{0} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxFakultas[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxFakultas[]"></td>
                @endif
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">2</td>
                <td>Scan ijazah luar negeri serta SK penyetaraan dari Ditjen DIKTI (jika lulusan luar negeri)</td>
                @if (($folder[1]))
                  <td class="w3-center"><a href="#" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow" style="pointer-events: none;">Berkas belum diunggah</a></td>
                @else
                  <td class="w3-center"><a href="/download/{{$calon->nip}}/1" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow">Unduh</a></td>
                @endif
                @if ($calon->statusBerkasSdm{1} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxSdm[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxSdm[]"></td>
                @endif
                @if ($calon->statusBerkasFakultas{1} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxFakultas[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxFakultas[]"></td>
                @endif
            </tr>


            <tr class="c2-background-grey">
              <td class="w3-center">3</td>
              <td>Scan abstrak Disertasi/ Thesis</td>
              @if (($folder[2]))
                <td class="w3-center"><a href="#" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow" style="pointer-events: none;">Berkas belum diunggah</a></td>
              @else
                <td class="w3-center"><a href="/download/{{$calon->nip}}/2" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow">Unduh</a></td>
              @endif
              @if ($calon->statusBerkasSdm{2} == "0")
                <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxSdm[]"></td>
              @else
                <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxSdm[]"></td>
              @endif
              @if ($calon->statusBerkasFakultas{2} == "0")
                <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxFakultas[]"></td>
              @else
                <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxFakultas[]"></td>
              @endif
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">4</td>
                <td>Scan surat keputusan pemberian tugas belajar, disahkan oleh pejabat yang berwenang (jika ada)</td>
                @if (($folder[3]))
                  <td class="w3-center"><a href="#" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow" style="pointer-events: none;">Berkas belum diunggah</a></td>
                @else
                  <td class="w3-center"><a href="/download/{{$calon->nip}}/3" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow">Unduh</a></td>
                @endif
                @if ($calon->statusBerkasSdm{3} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxSdm[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxSdm[]"></td>
                @endif
                @if ($calon->statusBerkasFakultas{3} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxFakultas[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxFakultas[]"></td>
                @endif
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">5</td>
                <td>Scan surat keputusan pengaktifan kembali setelah selesai melaksanakan tugas belajar, disahkan oleh pejabat berwenang (jika ada)</td>
                @if (($folder[4]))
                  <td class="w3-center"><a href="#" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow" style="pointer-events: none;">Berkas belum diunggah</a></td>
                @else
                  <td class="w3-center"><a href="/download/{{$calon->nip}}/4" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow">Unduh</a></td>
                @endif
                @if ($calon->statusBerkasSdm{4} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxSdm[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxSdm[]"></td>
                @endif
                @if ($calon->statusBerkasFakultas{4} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxFakultas[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxFakultas[]"></td>
                @endif
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">6</td>
                <td>Scan DUPAK yang telah ditandatangani oleh pejabat berwenang</td>
                @if (($folder[5]))
                <td class="w3-center"><a href="#" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow" style="pointer-events: none;">Berkas belum diunggah</a></td>
                @else
                <td class="w3-center"><a href="/download/{{$calon->nip}}/5" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow">Unduh</a></td>
                @endif
                @if ($calon->statusBerkasSdm{5} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxSdm[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxSdm[]"></td>
                @endif
                @if ($calon->statusBerkasFakultas{5} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxFakultas[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxFakultas[]"></td>
                @endif
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">7</td>
                <td>Scan PAK terakhir, disahkan oleh pejabat yang berwenang</td>
                @if (($folder[6]))
                  <td class="w3-center"><a href="#" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow" style="pointer-events: none;">Berkas belum diunggah</a></td>
                @else
                  <td class="w3-center"><a href="/download/{{$calon->nip}}/6" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow">Unduh</a></td>
                @endif
                @if ($calon->statusBerkasSdm{6} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxSdm[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxSdm[]"></td>
                @endif
                @if ($calon->statusBerkasFakultas{6} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxFakultas[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxFakultas[]"></td>
                @endif
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">8</td>
                <td>Scan surat keputusan jabatan terakhir, disahkan oleh pejabat yang berwenang</td>
                @if (($folder[7]))
                  <td class="w3-center"><a href="#" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow" style="pointer-events: none;">Berkas belum diunggah</a></td>
                @else
                  <td class="w3-center"><a href="/download/{{$calon->nip}}/7" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow">Unduh</a></td>
                @endif
                @if ($calon->statusBerkasSdm{7} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxSdm[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxSdm[]"></td>
                @endif
                @if ($calon->statusBerkasFakultas{7} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxFakultas[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxFakultas[]"></td>
                @endif
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">9</td>
                <td>Scan surat keputusan kenaikan pangkat terakhir, disahkan oleh pejabat yang berwenang</td>
                @if (($folder[8]))
                  <td class="w3-center"><a href="#" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow" style="pointer-events: none;">Berkas belum diunggah</a></td>
                @else
                  <td class="w3-center"><a href="/download/{{$calon->nip}}/8" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow">Unduh</a></td>
                @endif
                @if ($calon->statusBerkasSdm{8} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxSdm[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxSdm[]"></td>
                @endif
                @if ($calon->statusBerkasFakultas{8} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxFakultas[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxFakultas[]"></td>
                @endif
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">10</td>
                <td>Scan PPKP (Penilaian Prestasi Kerja Pegawai) dua tahun terakhir, disahkan pejabat berwenang</td>
                @if (($folder[9]))
                  <td class="w3-center"><a href="#" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow" style="pointer-events: none;">Berkas belum diunggah</a></td>
                @else
                  <td class="w3-center"><a href="/download/{{$calon->nip}}/9" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow">Unduh</a></td>
                @endif
                @if ($calon->statusBerkasSdm{9} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxSdm[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxSdm[]"></td>
                @endif
                @if ($calon->statusBerkasFakultas{9} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxFakultas[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxFakultas[]"></td>
                @endif
            </tr>

                <tr class="c2-background-grey">
                <td class="w3-center">11</td>
                <td>Scan Surat pernyataan melaksanakan penelitian</td>
                @if (($folder[10]))
                  <td class="w3-center"><a href="#" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow" style="pointer-events: none;">Berkas belum diunggah</a></td>
                @else
                  <td class="w3-center"><a href="/download/{{$calon->nip}}/10" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow">Unduh</a></td>
                @endif
                @if ($calon->statusBerkasSdm{10} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxSdm[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxSdm[]"></td>
                @endif
                @if ($calon->statusBerkasFakultas{10} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxFakultas[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxFakultas[]"></td>
                @endif
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">12</td>
                <td>Scan surat pernyataan Pengesahan Hasil Validasi Karya Ilmiah</td>
                @if (($folder[11]))
                  <td class="w3-center"><a href="#" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow" style="pointer-events: none;">Berkas belum diunggah</a></td>
                @else
                  <td class="w3-center"><a href="/download/{{$calon->nip}}/11" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow">Unduh</a></td>
                @endif
                @if ($calon->statusBerkasSdm{11} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxSdm[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxSdm[]"></td>
                @endif
                @if ($calon->statusBerkasFakultas{11} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxFakultas[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxFakultas[]"></td>
                @endif
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">13</td>
                <td>Berkas berisi tautan karya ilmiah (lihat lampiran 4, surat edaran 1864/2015 Dirjen Dikti)</td>
                @if (($folder[12]))
                  <td class="w3-center"><a href="#" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow" style="pointer-events: none;">Berkas belum diunggah</a></td>
                @else
                  <td class="w3-center"><a href="/download/{{$calon->nip}}/12" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow">Unduh</a></td>
                @endif
                @if ($calon->statusBerkasSdm{12} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxSdm[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxSdm[]"></td>
                @endif
                @if ($calon->statusBerkasFakultas{12} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxFakultas[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxFakultas[]"></td>
                @endif
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">14</td>
                <td>Berkas berisi hasil similarity checking untuk setiap karya ilmiah, di satukan dalam format zip. Setiap hasil similarity checking memuat nomor sesuai urutan karya ilmiah (lihat Surat Edaran Dirjen SDIPT KemenristekDikti ) No 1753/2016</td>
                @if (($folder[13]))
                  <td class="w3-center"><a href="#" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow" style="pointer-events: none;">Berkas belum diunggah</a></td>
                @else
                  <td class="w3-center"><a href="/download/{{$calon->nip}}/13" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow">Unduh</a></td>
                @endif
                @if ($calon->statusBerkasSdm{13} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxSdm[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxSdm[]"></td>
                @endif
                @if ($calon->statusBerkasFakultas{13} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxFakultas[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxFakultas[]"></td>
                @endif
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">15</td>
                <td>Scan Sertifikat Pendidik untuk Dosen</td>
                @if (($folder[14]))
                  <td class="w3-center"><a href="#" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow" style="pointer-events: none;">Berkas belum diunggah</a></td>
                @else
                  <td class="w3-center"><a href="/download/{{$calon->nip}}/14" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow">Unduh</a></td>
                @endif
                @if ($calon->statusBerkasSdm{14} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxSdm[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxSdm[]"></td>
                @endif
                @if ($calon->statusBerkasFakultas{14} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxFakultas[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxFakultas[]"></td>
                @endif
            </tr>

            <tr class="c2-background-grey">
                <td class="w3-center">16</td>
                <td>Scan setiap lembar peer review karya ilmiah ( berikan nama berkas sesuai daftar usulan dan simpan dalam satu folder)</td>
                @if (($folder[15]))
                  <td class="w3-center"><a href="#" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow" style="pointer-events: none;">Berkas belum diunggah</a></td>
                @else
                  <td class="w3-center"><a href="/download/{{$calon->nip}}/15" class="w3-btn w3-border w3-round-large c2-background-grey w3-text-white w3-hover-yellow">Unduh</a></td>
                @endif
                @if ($calon->statusBerkasSdm{15} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxSdm[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxSdm[]"></td>
                @endif
                @if ($calon->statusBerkasFakultas{15} == "0")
                  <td class="w3-center"><input class="w3-check" type="checkbox" name="checkboxFakultas[]"></td>
                @else
                  <td class="w3-center"><input class="w3-check" type="checkbox" checked="true" name="checkboxFakultas[]"></td>
                @endif
              </tr>

        </table>
    </div>

    <div class="w3-left">
        <p class="c2-text-grey c2-ssp-regular">Catatan: Seluruh berkas di atas, disimpan ke dalam CD dan dikirimkan ke Rektorat UP Direktorat SDM-UI, setelah diperiksa dikirim ke sekretariat DGB-UI</p>
    </div>
</div>

@endsection
