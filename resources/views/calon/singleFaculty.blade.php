@extends('layouts.app')

@section('content')

<div class="w3-container w3-padding-large">
  @if(session()->has('message'))
    <div id="id01" class="w3-modal" style="display:block">
        <div class="w3-modal-content w3-card-4 w3-green w3-round-large" style="width:300px;height:400px;text-align:center">
            <i class="fas fa-check" style="font-size:150px;"></i>
            <br>
            <h1>SUCCESS</h1>
            <br>
            <p>Calon berhasil dihapus</p>
            <button onclick="document.getElementById('id01').style.display='none'"
              class="w3-button w3-margin-bottom w3-hover-black w3-light-grey w3-text-grey w3-hover-opacity w3-border w3-round-large c2-ssp-regular w3-display-bottommiddle"
              style="width:80%">
              Ok
            </button>
        </div>
    </div>
  @endif
  <br>
  <h2 class="c2-domine w3-center">Lihat List Calon</h2>
  <br>
  <br>

    @foreach ($calons as $calon)
    <div class="w3-col l4 m12 w3-padding c2-ssp-regular">
      <a href="/calon/detail/{{$calon->nip}}" style="text-decoration:none;">
        <div class="w3-panel w3-card-2 w3-white w3-round-large w3-margin" style="padding:10px;">
          <img class="w3-image responsive w3-left" src="{{asset('assets/img/calon.png')}}" style="width:10%;margin-top:10px">
          <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$calon->name}}</p>
        </div>
      </a>
    </div>
    @endforeach
</div>

@endsection
