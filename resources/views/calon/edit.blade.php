@extends('layouts.app')

@section('content')
  
<h2 class="c2-text-grey c2-domine" style="text-align:center;margin-top:40px">Edit Calon</h2>
<form class="w3-container" method="POST" action="/calon/edit" enctype="multipart/form-data">
<div class="w3-container" style="max-width:400px;padding:10px;margin:auto;margin-top:30px">
    <div class="w3-round-large c2-background-grey w3-padding-16">
        <input type="text" name="id" value="{{$calon->id}}" class="w3-hide">
        <div class="w3-padding">
            <input id="namalengkap" class="w3-text-white w3-border-bottom c2-background-grey w3-input w3-border-yellow c2-width-100"  type="text" placeholder="Nama Lengkap Dosen" name="name" value="{{$calon->name}}" required autofocus></input>
        </div>
        <div class="w3-padding">
            <input id="nip-dosen" class="w3-text-white w3-border-bottom c2-background-grey w3-input w3-border-yellow c2-width-100" type="text" placeholder="NIP/NUP" name="nip" value="{{$calon->nip}}" required></input>
        </div>
        <div class="w3-padding w3-padding-16">
            <select id="faculty_id" class="w3-text-white w3-border-bottom c2-background-grey w3-input w3-border-yellow c2-width-100" name="faculty_id" required>
                <option value="{{$calon->faculty_id}}" selected>{{$facultyName}}</option>
                @foreach ($faculties as $faculty)
                  @if ($calon->faculty_id != $faculty->id)
                      <option value="{{$faculty->id}}">{{$faculty->name}}</option>
                  @endif
                @endforeach
            </select>
        </div>
        <div class="w3-padding w3-padding-16">
            <select id="usulan" class="w3-text-white w3-border-bottom  c2-background-grey w3-input w3-border-yellow c2-width-100" name="usulan" required>
                <option value="1" @if ($calon->usulan == 1) selected @endif>Lektor Kepala ke Guru Besar</option>
                <option value="2" @if ($calon->usulan == 2) selected @endif>Lektor ke Guru Besar</option>
                <option value="3" @if ($calon->usulan == 3) selected @endif>Lektor ke Lektor Kepala</option>
                <option value="4" @if ($calon->usulan == 4) selected @endif>Asisten Ahli ke Lektor Kepala</option>
                <option value="5" @if ($calon->usulan == 5) selected @endif>Jabatan yang sama</option>
            </select>
        </div>
    </div>
    <br>
    <div class="w3-center">
        {{csrf_field()}}
        <button type="submit" name="button" class="w3-button w3-hover-black c2-background-grey w3-text-white w3-border w3-round-xlarge c2-ssp-regular">Submit</button>
    </div>
</div>
</form>

@endsection
