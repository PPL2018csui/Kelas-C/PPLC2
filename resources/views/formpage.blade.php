<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Form dummy page</title>
  </head>
  <body>
    <h1>Dummy Page for Form</h1>
    <li><a href="form/form1">Formulir 1: Penilaian usulan kenaikan jabatan fungsional dosen dari Lektor Kepala ke Guru Besar</a></li>
    <li><a href="form/form2">Formulir 2: Penilaian usulan kenaikan Loncat jabatan fungsional dosen dari Lektor ke Guru Besar</a></li>
    <li><a href="form/form3">Formulir 3: Penilaian usulan kenaikan Regular jabatan fungsional dosen dari Lektor ke Lektor Kepala</a></li>
    <li><a href="form/form4">Formulir 4: Penilaian usulan kenaikan Loncat jabatan fungsional dosen dari Asisten Ahli ke Lektor Kepala</a></li>
    <li><a href="form/form5">Formulir 5: Penilaian usulan kenaikan pangkat dalam Jabatan yang sama</a></li>
  </body>
</html>
