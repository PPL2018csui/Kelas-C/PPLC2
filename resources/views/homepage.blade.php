@extends('layouts.app')

@section('content')
<div class="w3-container w3-padding-large">
    <br>
    <h1 class="w3-center c2-domine">Selamat Datang di EasyJAD</h1>
    <br>
    <br>
    <div class="w3-row w3-center">
        <div class="w3-row w3-center">
          @if(Auth::user()->role == 1 || Auth::user()->role == 2)
            <div class="c2-home-first-row">
                <div class="w3-col l4 m12 w3-container">
                    <div class="c2-home-box-style">
                        <a class="c2-remove-link-underline" href="/calon/create">
                            <div class="w3-panel w3-card-2 w3-border w3-round-large w3-white">
                              <img class="c2-home-image-style responsive" src="{{ asset('assets/img/tambahCalon.png') }}"  alt="tambahCalon.png">
                              <h4>Tambah Calon</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="w3-col l4 m12 w3-container">
                    <div class="c2-home-box-style">
                        <a class="c2-remove-link-underline" href="#">
                            <div class="w3-panel w3-card-2 w3-border w3-round-large w3-white">
                                <img class="c2-home-image-style responsive" src="{{ asset('assets/img/lihatHasilPenilaian.png') }}" >
                                <h4>Lihat Hasil Penilaian</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="w3-col l4 m12 w3-container">
                    <div class="c2-home-box-style">
                        <a class="c2-remove-link-underline" href="/calon">
                            <div class="w3-panel w3-card-2 w3-border w3-round-large w3-white">
                                <img class="c2-home-image-style responsive" src="{{ asset('assets/img/lihatListCalon.svg') }}">
                                <h4>Lihat List Calon</h4>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
          @endif
          @if(Auth::user()->role == 3 || Auth::user()->role == 4)
          <div class="c2-home-second-row">
            <div class="w3-col l6 m12 w3-container">
                <div class="c2-home-box-style">
                  <a class="c2-remove-link-underline" href="#">
                      <div class="w3-panel w3-card-2 w3-border w3-round-large w3-white">
                          <img class="c2-home-image-style responsive" src="{{ asset('assets/img/lihatHasilPenilaian.png') }}" >
                          <h4>Lihat Hasil Penilaian</h4>
                      </div>
                  </a>
                </div>
            </div>
            <div class="w3-col l6 m12 w3-container">
                <div class="c2-home-box-style">
                  <a class="c2-remove-link-underline" href="/calon">
                      <div class="w3-panel w3-card-2 w3-border w3-round-large w3-white">
                          <img class="c2-home-image-style responsive" src="{{ asset('assets/img/lihatListCalon.svg') }}">
                          <h4>Lihat List Calon</h4>
                      </div>
                  </a>
                </div>
            </div>

          </div>
          @endif
        </div>
    </div>
<div class="c2-add-spacing"></div>
    <div class="w3-row w3-center">
        <div class="w3-rest w3-container">

            <div align="center" class="w3-row w3-center">
              <div class="c2-home-second-row">
                @if(Auth::user()->role == 1)
                <div class="w3-col l6 m12 w3-container">
                    <div class="c2-home-box-style">
                        <a class="c2-remove-link-underline" href="/register">
                          <div class="w3-panel w3-card-2 w3-border w3-round-large w3-white">
                              <img class="c2-home-image-style responsive" src="{{ asset('assets/img/buatUserBaru.png') }}" style="width:80%;">
                              <h4 class="c2-ssp-reguler">Buat User Baru</h4>
                          </div>
                      </a>
                    </div>
                </div>
                @endif

                @if(Auth::user()->role == 1)
                  <div class="w3-col l6 m12 w3-container">
                      <div class="c2-home-box-style">
                          <a class="c2-remove-link-underline" href="/managefaculty" dusk="kelola">
                              <div class="w3-panel w3-card-2 w3-border w3-round-large w3-white">
                                  <img class="c2-home-image-style responsive" src="{{ asset('assets/img/kelolaFakultas.png') }}">
                                  <h4 class="c2-ssp-reguler">Kelola Fakultas</h4>
                              </div>
                          </a>
                      </div>
                  </div>
                @endif

              </div>
            </div>
        </div>
    </div>
</div>
@endsection
