@extends('layouts.form')

@section('content')
{{-- Form Head start --}}
<p style="text-align:center">Komite 5 (promosi dan demosi) Dewan Guru Besar – Univesitas Indonesia</p>
<p style="text-align:center">Email: dgb-ui- komite5@googlegroups.com</p>
<p style="text-align:center">Formulir 5: Penilaian usulan kenaikan pangkat dalam Jabatan yang sama</p>
<p style="text-align:center">===========================================================================</p>
<p>Nama Dosen yang diusulkan<span style="margin-left:20px">:</span> {{$calon->name}}</p>
<p>NIP<span style="margin-left:178px">:</span> {{$calon->nip}} <span  style="margin-left:150px;">Fakultas<span>:</span> {{$facultyName}}</span></p>
<p>Pendidikan terakhir<span style="margin-left:78px;">:  ...............................</span> <span style="margin-left:98px;">Jabatan terakhir: .....................</span><p>
<p>Pangkat Terakhir<span style="margin-left:93px;">: ...............................</span> <span style="margin-left:98px;">Tanggal penilaian: ....................</span></p>
{{-- Form Head end --}}

{{-- Form Body start --}}
<table style="border-collapse: collapse;">
        <tr>
            <td style="border: 1px solid black;padding-left:10px;padding-right:10px;text-align:center;">No</td>
            <td style="border: 1px solid black;padding-left:10px;padding-right:10px;text-align:center;">Syarat kenaikan pangkat (Permendikbud No 92 Tahun 2014, Pasal 12).</td>
            <td style="border: 1px solid black;padding-left:10px;padding-right:10px;text-align:center;">Hasil penilaian (ya/tidak) memenuhi</td>
        </tr>
        {{-- No 1 --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">1</td>
            <td style="border: 1px solid black;">Ayat 1<br><br>
            Kenaikan pangkat dapat dilakukan apabila paling singkat 2 (dua) tahun dalam pangkat terakhir.</td>
            <td style="border: 1px solid black;">Lama pangkat terakhir ......... <br>
            Tahun</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum.</td>
            <td style="border: 1px solid black;"></td>
        </tr>

        {{-- No 2 --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">2</td>
            <td style="border: 1px solid black;">Ayat 2, butir a <br><br>
            Telah memenuhi angka kredit yang dipersyaratkan baik secara kumulatif maupun setiap unusr kegiatan pada lingkup jabatan tersebut.</td>
            <td style="border: 1px solid black;">Isi ya atau tidak, berapa angka kredit yang telah dikumpulkan ........</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum.</td>
            <td style="border: 1px solid black;"></td>
        </tr>

        <tr>
            <td style="border: 1px solid black;"></td>
            <td style="border: 1px solid black;"><b>Bagi Lektor atau Lektor Kepala</b></td>
            <td style="border: 1px solid black;"></td>
        </tr>

        {{-- No 3 --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">3</td>
            <td style="border: 1px solid black;">Ayat 2, butir b <br><br>
            Memiliki karya ilmiah yang dipublikasikan dalam jurnal ilmiah nasional dan/atau internasional untuk jabatan Lektor dan Lektor Kepala sebagai penulis utama</td>
            <td style="border: 1px solid black;text-align:center;">Isi ya atau tidak</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Catatan: tuliskam di jurnal apa dan apa judul makalahnya</td>
            <td style="border: 1px solid black;"></td>
        </tr>

        <tr>
            <td style="border: 1px solid black;"></td>
            <td style="border: 1px solid black;"><b>Bagi Guru Besar</b></td>
            <td style="border: 1px solid black;"></td>
        </tr>

        {{-- No 4 --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">4</td>
            <td style="border: 1px solid black;">Ayat 2, butir c <br><br>
            Memiliki karya ilmiah yang dipublikasikan dalam jurnal ilmiah nasional terakreditasi untuk jabatan Profesor sebagai penulis utama</td>
            <td style="border: 1px solid black;text-align:center;">Isi ya atau tidak</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Catatan: tuliskan di jurnal apa dan apa judul makalahnya..</td>
            <td style="border: 1px solid black;"></td>
        </tr>

        {{-- No 5 --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">5</td>
            <td style="border: 1px solid black;">Ayat 3 <br><br>
            Dosen yang memperoleh kenaikan jabatan secara reguler namun pangkatnya masih dalam lingkup jabatan sebelumnya, maka untuk kenaikan pangkat berikutnya
          tidak disyaratkan tambahan angka kredit sampai pada pangkat maksimum dalam lingkup jabatan tersebut apabila jumlah angka kredit yang ditetapkan memenuhi.</td>
            <td style="border: 1px solid black;text-align:center;">Isi ya atau tidak</td>
        </tr>

        {{-- No 6 --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">6</td>
            <td style="border: 1px solid black;">Ayat 4 <br><br>
              Dosen yang telah memperoleh kenaikan jabatan secara loncat jabatan, maka kenaikan pangkat berikutnya sampai pada pangkat maksimum dalam lingkup jabatan setingkat lebih tinggi dari
              jabatan semula tidak lagi disyaratkan tambahan angka kredit, sedangkan untuk kenaikan pangkat sampai pada pangkat maksimum dalam lingkup jabatan yang diperoleh melalui loncat
              jabatan sesuai dengan jumlah angka kredit yang telah ditetapkan, wajib mengumpulkan tambahan angka kredit sebanyak 30% dari unsur utama yang disyaratkan untuk kenaikan pangkat tersebut.</td>
            <td style="border: 1px solid black;">Periksa apakah jabatan terakhir karena loncat jabatan, dan periksa apakah syarat ini dipenuhi. <br><br>
            Loncat: ya atau tidak <br><br>
          Jika loncat jabatan, apakah tambahan angka kredit 30% telah dipenuhi: ya atau tidak</td>
        </tr>

        {{-- No 7 --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">7</td>
            <td style="border: 1px solid black;">Surat Edaran No 1864 ttd 1 Oktober 2015 Dirjen Sumber Daya IPTEK dan DIKTI, Kementrian RisTek dan Dikti tentang kelengkapan lampiran untuk proses on-line. (DGB memeriksa terutama link karya ilmiah, lampiran 4 ttg
              Surat Pernyataan Keabsahan Karya Ilmiah, Lampiran – Peer Review Karya Ilmiah ) dan sertfikat Dosen Kelengkapan lain biasanya disediakan oleh SDM.</td>
            <td style="border: 1px solid black;text-align:center;">Isi ya atau tidak</td>
        </tr>

        {{-- No 8 --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">8</td>
            <td style="border: 1px solid black;">Catatan lainnya: tuliskan catatan lainnya disini.</td>
            <td style="border: 1px solid black;padding-top:80px;"></td>
        </tr>

        <tr>
            <td style="border: 1px solid black;"></td>
            <td style="border: 1px solid black;">Penilai 1</td>
            <td style="border: 1px solid black;text-align:center;">Penilai 2</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Ttd</td>
            <td style="border: 1px solid black;text-align:center;">Ttd</td>
        </tr>

        <tr>
            <td style="border: 1px solid black;"></td>
            <td style="border: 1px solid black;">(nama lengkap)</td>
            <td style="border: 1px solid black;text-align:center;">(nama lengkap)</td>
        </tr>
</table>
{{-- Form Body end --}}
@endsection
