@extends('layouts.form')

@section('content')
{{-- Form Head start --}}
<p style="text-align:center">Komite 5 (promosi dan demosi) Dewan Guru Besar – Univesitas Indonesia</p>
<p style="text-align:center">Email: dgb-ui- komite5@googlegroups.com</p>
<p style="text-align:center">Formulir 4: Penilaian usulan kenaikan Loncat jabatan fungsional dosen dari Asisten Ahli ke Lektor Kepala</p>
<p style="text-align:center">===========================================================================</p>
<p>Nama Dosen yang diusulkan<span style="margin-left:20px">:</span> {{$calon->name}}</p>
<p>NIP<span style="margin-left:178px">:</span> {{$calon->nip}}</p>
<p>Pendidikan terakhir<span style="margin-left:78px;">:  S2................, S3................</span></p>
<p>Fakultas<span style="margin-left:150px">:</span> {{$facultyName}} <span style="margin-left:98px;">Tanggal penilaian: ....................</span></p>
{{-- Form Head end --}}

{{-- Form Body start --}}
<table style="border-collapse: collapse;">
        <tr>
            <td style="border: 1px solid black;padding-left:10px;padding-right:10px;text-align:center;">No</td>
            <td style="border: 1px solid black;padding-left:10px;padding-right:10px;text-align:center;">Syarat kenaikan jabatan Loncat jabatan dari Asisten Ahli ke Lektor Kepala (Permendikbud No 92 Tahun 2014, Pasal 11 ayat 2).</td>
            <td style="border: 1px solid black;padding-left:10px;padding-right:10px;text-align:center;">Hasil penilaian (ya/tidak) memenuhi</td>
        </tr>
        {{-- No 1a --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">1.a</td>
            <td style="border: 1px solid black;">Paling singkat 2 (dua) tahun menduduki jabatan Asisten Ahli.</td>
            <td style="border: 1px solid black;text-align:center;">Isi ya atau tidak</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum.</td>
            <td style="border: 1px solid black;"></td>
        </tr>

        {{-- No 1b --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">1.b</td>
            <td style="border: 1px solid black;">Memiliki ijazah Doktor (S3)</td>
            <td style="border: 1px solid black;text-align:center;">Isi ya atau tidak</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum.</td>
            <td style="border: 1px solid black;"></td>
        </tr>

        {{-- No 1c --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">1.c</td>
            <td style="border: 1px solid black;">Memiliki paling sedikit 2 (dua) karya ilmiah yang dipublikasikan pada jurnal ilmiah
            internasional bereputasi sebagai penulis pertama</td>
            <td style="border: 1px solid black;text-align:center;">Isi ya atau tidak</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum
              dengan menuliskan apakah ada judul karya ilmiah yang memenuhi syarat tersebut.</td>
            <td style="border: 1px solid black;"></td>
        </tr>

        {{-- No 1d --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">1.d</td>
            <td style="border: 1px solid black;">Memenuhi syarat-syarat lainnya sebagaimana dimaksud Pasal 9 ayat (1) huruf b. yakni telah memenuhi
            angka kredit yang dipersyaratkan baik secara kumulatif maupun setiap unsur kegiatan sesuai dengan Lampiran I (AK Pedidikan min 40%,
            AK Penelitian min 40%, AK PKN maks 10%, AK Pendukung maks 10%). Lihat juga komposisi nilai pada Lampiran II Permenpan No 46 Tahun 2013</td>
            <td style="border: 1px solid black;text-align:center;">Isi ya atau tidak</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Catatan: jelaskan apakah alasan penilaian apakah syarat sudah dipenuhi atau belum.</td>
            <td style="border: 1px solid black;"></td>
        </tr>

        {{-- No 2 --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">2</td>
            <td style="border: 1px solid black;">Prosentase karya ilmiah Tabel 6, Pedoman Operasional PAK DIKTI (Karya Ilmiah dalam
              a. Jurnal Nasional, b. Prosiding Nasional, c. Poster dan Prosiding Nasional - masing-masing maksimum 25% dari AK yang dibutuhkan di penelitian;
              Karya Ilmiah dalam Koran/majalah atau tak dipublikasikan maksimum 5% dari AK yang dibutuhkan).</td>
            <td style="border: 1px solid black;text-align:center;">Isi ya atau tidak</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Catatan: jelaskan apakah prosentase jumlah nilai masing-masing kategori sesuai ketentuan</td>
            <td style="border: 1px solid black;"></td>
        </tr>

        {{-- No 3 --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">3</td>
            <td style="border: 1px solid black;">Surat Edaran No 1864 ttd 1 Oktober 2015 Dirjen Sumber Daya IPTEK dan DIKTI,
             Kementrian RisTek dan Dikti tentang kelengkapan lampiran untuk proses on-line. (DGB memeriksa terutama link karya ilmiah,
             lampiran 4 ttg Surat Pernyataan Keabsahan Karya Ilmiah, Lampiran - Peer Review Karya Ilmiah dan serifikat Dosen.
              Kelengkapan lain biasanya disediakan oleh SDM)</td>
            <td style="border: 1px solid black;text-align:center;">Isi ya atau tidak</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Catatan: tuliskan kelengkapan tersebut disini</td>
            <td style="border: 1px solid black;"></td>
        </tr>

        {{-- No 4 --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">4</td>
            <td style="border: 1px solid black;">Catatan: tuliskan catatan lainnya disini</td>
            <td style="border: 1px solid black;padding-top:80px;"></td>
        </tr>

        <tr>
            <td style="border: 1px solid black;"></td>
            <td style="border: 1px solid black;">Penilai 1</td>
            <td style="border: 1px solid black;text-align:center;">Penilai 2</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Ttd</td>
            <td style="border: 1px solid black;text-align:center;">Ttd</td>
        </tr>

        <tr>
            <td style="border: 1px solid black;"></td>
            <td style="border: 1px solid black;">(nama lengkap)</td>
            <td style="border: 1px solid black;text-align:center;">(nama lengkap)</td>
        </tr>
</table>
{{-- Form Body end --}}
@endsection
