@extends('layouts.form')

@section('content')
{{-- Form Head start --}}
<p style="text-align:center">Komite 5 (promosi dan demosi) Dewan Guru Besar – Univesitas Indonesia</p>
<p style="text-align:center">Email: dgb-ui- komite5@googlegroups.com</p>
<p style="text-align:center">Formulir 1: Penilaian usulan kenaikan jabatan fungsional dosen dari Lektor Kepala ke Guru Besar</p>
<p style="text-align:center">===========================================================================</p>
<p>Nama Dosen yang diusulkan<span style="margin-left:20px">:</span> {{$calon->name}}</p>
<p>NIP<span style="margin-left:178px">:</span> {{$calon->nip}}</p>
<p>Pendidikan terakhir<span style="margin-left:78px;">:  S3...........................(ya/tidak).....</span> tanggal ijazah:................................</p>
<p>Fakultas<span style="margin-left:150px">:</span> {{$facultyName}} <span style="margin-left:98px;">Tanggal penilaian: ....................</span></p>
{{-- Form Head end --}}

{{-- Form Body start --}}
<table style="border-collapse: collapse;">
        <tr>
            <td style="border: 1px solid black;padding-left:10px;padding-right:10px;text-align:center;">No</td>
            <td style="border: 1px solid black;padding-left:10px;padding-right:10px;text-align:center;">Syarat kenaikan jabatan regular ke Guru Besar (Permendikbud No 92 Tahun 2014, Pasal 10 ayat 1)</td>
            <td style="border: 1px solid black;padding-left:10px;padding-right:10px;text-align:center;">Hasil penilaian (ya/tidak) memenuhi</td>
        </tr>
        {{-- No 1a --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">1.a</td>
            <td style="border: 1px solid black;">Memiliki pengalaman kerja sebagai dosen tetap paling singkat 10(sepuluh) tahun</td>
            <td style="border: 1px solid black;text-align:center;">Isi ya atau tidak</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum</td>
            <td style="border: 1px solid black;"></td>
        </tr>

        {{-- No 1b --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">1.b</td>
            <td style="border: 1px solid black;">Memiliki kualifikasi akademik doktor (S3)</td>
            <td style="border: 1px solid black;text-align:center;">Isi ya atau tidak</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum</td>
            <td style="border: 1px solid black;"></td>
        </tr>

        {{-- No 1c --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">1.c</td>
            <td style="border: 1px solid black;">Paling singkat 3 (tahun) setelah memperoleh ijazah doktor (S3)
              (ada pengecualian bagi yang mempunyai publikasi di jurnal
              internasional berputasi sebagai penulis pertama setelah lulus S3)</td>
            <td style="border: 1px solid black;text-align:center;">Isi ya atau tidak</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum</td>
            <td style="border: 1px solid black;"></td>
        </tr>

        {{-- No 1d --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">1.d</td>
            <td style="border: 1px solid black;">Paling singkat 2 (dua) tahun menduduki jabatan Lektor Kepala</td>
            <td style="border: 1px solid black;text-align:center;">Isi ya atau tidak</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum</td>
            <td style="border: 1px solid black;"></td>
        </tr>

        {{-- No 1e --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">1.e</td>
            <td style="border: 1px solid black;">Telah memenuhi angka kredit yang dipersyaratkan baik secara kumulatif
              maupun setiap unsur kegiatan sesuai dengan Lampiran I; (AK pendidikan
              min 35%, AK Penelitian min 45%, AK PKM maks 10%, Penunjang maks
              10%). Periksa juga Lampiran III dan IV, Permenpan no 17 tahun 2013.</td>
            <td style="border: 1px solid black;text-align:center;">Isi ya atau tidak</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum</td>
            <td style="border: 1px solid black;"></td>
        </tr>

        {{-- No 1f --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">1.f</td>
            <td style="border: 1px solid black;">Memiliki karya ilmiah yang dipublikasikan dalam jurnal ilmiah
              internasional bereputasi sebagai penulis pertama</td>
            <td style="border: 1px solid black;text-align:center;">Isi ya atau tidak</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum</td>
            <td style="border: 1px solid black;"></td>
        </tr>

        {{-- No 1g --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">1.g</td>
            <td style="border: 1px solid black;">memiliki kinerja, integritas, etika dan tata krama, serta tanggung jawab</td>
            <td style="border: 1px solid black;text-align:center;">Isi ya atau tidak</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum</td>
            <td style="border: 1px solid black;"></td>
        </tr>

        {{-- No 2 --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">2</td>
            <td style="border: 1px solid black;">Memenuhi Matrik Keterkaitan Bidang Ilm S3, Bidang Ilmu Karya Ilmiah dengan Bidang Ilmu Penugasan Profesor.
              (Pedoman Operasional PAK DIKTI)</td>
            <td style="border: 1px solid black;text-align:center;">Isi ya atau tidak</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum.</td>
            <td style="border: 1px solid black;"></td>
        </tr>

        {{-- No 3 --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">3</td>
            <td style="border: 1px solid black;">Prosentase karya ilmiah Tabel 6, Pedoman Operasional PAK DIKTI
              (Karya Ilmiah dalam a. Jurnal Nasional, b. Prosiding Nasional, c. Poster dan Prosiding Nasional - masing-masing maksimum 25% dari AK yang dibutuhkan di penelitian;
               Karya Ilmiah dalam Koran/majalah atau tak dipublikasikan maksimum 5% dari AK yang dibutuhkan).</td>
            <td style="border: 1px solid black;text-align:center;">Isi ya atau tidak</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Catatan: jelaskan apakah prosentase jumlah nilai masing-masing kategori sesuai ketentuan.</td>
            <td style="border: 1px solid black;"></td>
        </tr>

        {{-- No 4 --}}
        <tr>
            <td style="border: 1px solid black;text-align:center;">4</td>
            <td style="border: 1px solid black;">Surat Edaran No 1864 ttd 1 Oktober 2015 Dirjen Sumber Daya IPTEK dan DIKTI,
             Kementrian RisTek dan Dikti tentang kelengkapan lampiran untuk proses on-line. (DGB memeriksa terutama link karya ilmiah,
             lampiran 4 ttg Surat Pernyataan Keabsahan Karya Ilmiah, Lampiran - Peer Review Karya Ilmiah dan serifikat Dosen.
              Kelengkapan lain biasanya disediakan oleh SDM)</td>
            <td style="border: 1px solid black;text-align:center;">Isi ya atau tidak</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Catatan: tuliskan kelengkapan tersebut disini.</td>
            <td style="border: 1px solid black;"></td>
        </tr>

        {{-- No 5 --}}
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;text-align:center;">5</td>
            <td style="border: 1px solid black;">Catatan lainnya: tuliskan catatan lainnya disini.</td>
            <td style="border: 1px solid black;padding-top:80px;"></td>
        </tr>

        <tr>
            <td style="border: 1px solid black;"></td>
            <td style="border: 1px solid black;">Penilai 1</td>
            <td style="border: 1px solid black;text-align:center;">Penilai 2</td>
        </tr>
        <tr style="padding-top:20px">
            <td style="border: 1px solid black;padding-top:80px;"></td>
            <td style="border: 1px solid black;">Ttd</td>
            <td style="border: 1px solid black;text-align:center;">Ttd</td>
        </tr>

        <tr>
            <td style="border: 1px solid black;"></td>
            <td style="border: 1px solid black;">(nama lengkap)</td>
            <td style="border: 1px solid black;text-align:center;">(nama lengkap)</td>
        </tr>
</table>
{{-- Form Body end --}}
@endsection
