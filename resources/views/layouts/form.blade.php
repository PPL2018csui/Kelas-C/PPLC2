<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Formulir Penilaian</title>
    <style media="screen">
        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid black;
        }
    </style>
  </head>
  <body>
    @yield('content')
  </body>
</html>
