<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/w3.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fontawesome-all.css') }}" rel="stylesheet">
</head>
<body>
    <div>
        <div class="w3-bar c2-background-grey">
            <div class="w3-container">
                <a class="w3-hover-opacity w3-left w3-text-white" href="{{ url('/home') }}" style="text-decoration:none">
                    <span class="c2-domine" style="font-size:50px">{{ config('app.name') }}</span>
                </a>

                <div>
                    <!-- Right Side Of Navbar -->
                    <div class="w3-right">
                                {{-- <li><a href="{{ route('login') }}">{{ __('Login') }}</a></li>
                                <li><a href="{{ route('register') }}">{{ __('Create User') }}</a></li> --}}
                        <div class="w3-right">
                            <div style="padding-top:12px">
                                <a onclick="myFunction()" class="w3-text-white w3-hover-opacity"><i class="far fa-user-circle" style="font-size:50px"></i></a>
                                <div id="Demo" class="w3-dropdown-content w3-bar-block w3-border" style="margin-left:-50px;margin-top:13px">
                                    <a href="{{ route('logout') }}" class="w3-bar-item w3-button" onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                         @csrf
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @yield('content')
    </div>

    <script>
        function myFunction() {
            var x = document.getElementById("Demo");
            if (x.className.indexOf("w3-show") == -1) {
                x.className += " w3-show";
            } else {
                x.className = x.className.replace(" w3-show", "");
            }
        }

        $("#role").change(function(){
            if($(this).val() == 2) {
                $("#fac").show();
            }else {
                $("#fac").hide();
            }
        });
    </script>
</body>
</html>
