<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/w3.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fontawesome-all.css') }}" rel="stylesheet">
</head>
<body>
  <div id="login">
        <main class="py-4">
          <div class="w3-container" style="max-width:400px;padding:10px;margin:auto;margin-top:200px">
          <div class="w3-round-large c2-background-grey w3-padding-16">

            <div class="w3-container">
              <img class="w3-left" src="{{asset('assets/img/logo_UI.png')}}" style="width:70px">
              <h1 class="c2-domine w3-center w3-text-white" style="margin-right:50px">EasyJAD</h1>
            </div>

            <hr style="width:100%;">

            <form method="POST" action="{{ route('login') }}" class="w3-container w3-padding-16">
              {{ csrf_field() }}
              <div>

                <input id = "username" type="text" name="username" placeholder="Username"
                class="w3-input w3-border w3-round-large {{ $errors->has('email') ? 'w3-border-red' : '' }}">
                @if ($errors->has('username'))
                  <span class="w3-text-red">
                    <strong>{{ $errors->first('username') }}</strong>
                  </span>
                @endif
              </div>

              <br>

              <div class="w3-padding-16">

                <input id="password" type="password" name="password" placeholder="Password"
                class="w3-input w3-border w3-round-large {{ $errors->has('password') ? 'w3-border-red' : '' }}">
                @if ($errors->has('password'))
                  <span class="w3-text-red">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
                @endif
              </div>

              <br>

              <div class="w3-container w3-center">
                <button dusk="login-button" type="submit" class="w3-btn w3-yellow w3-round-large" style="width:40%">
                  <strong>Log In</strong>
                </button>
              </div>
            </form>
          </div>
        </div>
        </main>
    </div>

    <script>
        function myFunction() {
            var x = document.getElementById("Demo");
            if (x.className.indexOf("w3-show") == -1) {
                x.className += " w3-show";
            } else {
                x.className = x.className.replace(" w3-show", "");
            }
        }
    </script>
</body>
</html>
