@extends('layouts.app')

@section('content')
  @if(session()->has('message'))
      <div id="id01" class="w3-modal" style="display:block">
          <div class="w3-modal-content w3-card-4 w3-green w3-round-large" style="width:300px;height:400px;text-align:center">
              <i class="fas fa-check" style="font-size:150px;"></i>
              <br>
              <h1>SUCCESS</h1>
              <br>
              <p class="w3-padding">{{ session()->get('message') }}</p>
              <button onclick="document.getElementById('id01').style.display='none'"
                class="w3-button w3-margin-bottom w3-hover-black w3-light-grey w3-text-grey w3-hover-opacity w3-border w3-round-large c2-ssp-regular w3-display-bottommiddle"
                style="width:80%">
                Ok
              </button>
          </div>
      </div>
  @endif
<h2 class="c2-text-grey c2-domine" style="text-align:center;margin-top:40px">{{__('Buat User Baru')}}</h2>

<div class="w3-border w3-round-large c2-background-grey c2-ssp-regular" style="max-width:400px;margin:auto;margin-top:30px">
    <div class="w3-center">
        <form method="POST" action="{{ route('register') }}" style="margin-top:50px;margin-bottom:50px">
            @csrf
            <div class="w3-padding">
                <input id="name" class="w3-text-white w3-border-bottom c2-background-grey w3-input w3-border-yellow c2-width-100 {{ $errors->has('name') ? ' w3-border-red' : '' }}"
                  placeholder="Nama Lengkap" type="text" name="name" value="{{ old('name') }}" required autofocus>

                @if ($errors->has('name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="w3-padding">
                <input id="user" class="w3-text-white w3-border-bottom c2-background-grey w3-input w3-border-yellow c2-width-100"
                  placeholder="Username" type="text" name="username" value="{{ old('username') }}" required>

                @if ($errors->has('username'))
                    <span class="w3-text-red">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
            </div>

            <div class="w3-padding">
                <input id="email" class="w3-text-white w3-border-bottom c2-background-grey w3-input w3-border-yellow c2-width-100"  placeholder="Email" type="email"
                  name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="w3-text-red">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="w3-padding">
                <select id="role" class="w3-text-light-grey w3-border-bottom c2-background-grey w3-input w3-border-yellow c2-width-100" name="role" required>
                    <option value="" disabled selected>Pilih Role</option>
                    <option value="1">Admin</option>
                    <option value="2">SDM Fakultas</option>
                    <option value="3">SDM UI</option>
                    <option value="4">DGB UI</option>
                </select>

                @if ($errors->has('role'))
                    <span class="w3-text-red">
                        <strong>{{ $errors->first('role') }}</strong>
                    </span>
                @endif
            </div>

            <div id="fac" class="w3-padding" style="display:none">
                <select id="faculty" class="w3-text-light-grey w3-border-bottom c2-background-grey w3-input w3-border-yellow c2-width-100" name="faculty">
                    <option value="null" disabled selected>Pilih Fakultas</option>

                    @foreach ($faculties as $faculty)
                        <option value="{{$faculty->id}}">{{$faculty->name}}</option>
                    @endforeach
                </select>

                @if ($errors->has('faculty'))
                    <span class="w3-text-red">
                        <strong>{{ $errors->first('faculty') }}</strong>
                    </span>
                @endif
            </div>



            <div class="w3-padding">
                <input id="password" class="w3-text-white w3-border-bottom c2-background-grey w3-input w3-border-yellow c2-width-100"  placeholder="Password" type="password"
                  name="password" required>

                @if ($errors->has('password'))
                    <span class="w3-text-red">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="w3-padding">
                <input id="password-confirm" class="w3-text-white w3-border-bottom c2-background-grey w3-input w3-border-yellow c2-width-100"  placeholder="Konfirmasi Password" type="password"
                  name="password_confirmation" required>
            </div>

            <div class="w3-padding">
                <button type="submit" class="w3-padding c2-background-yellow w3-hover-opacity w3-round-large c2-width-100">
                    {{ __('Create User') }}
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
