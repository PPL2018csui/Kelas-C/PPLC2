@extends('layouts.app')

@section('content')

<div class="w3-container" style="max-width:1500px;padding:10px;margin:auto;margin-top:20px">

    <p class="c2-domine w3-center">Komite 5 (promosi dan demosi) Dewan Guru Besar - Universitas Indonesia </p>
    <p class="c2-domine w3-center">Email: dgb-ui-komite5@googlegroups.com</p>
    <br>
    <p class="c2-domine w3-center">Formulir 2: Penilaian usulan kenaikan Loncat jabatan fungsional dosen dari Lektor ke Guru Besar</p>
    <p class="c2-domine w3-center">=================================================================================================</p>

    <div class="w3-container" style="margin-left:57px;margin-top:30px">

        <form>

            <div class="w3-row w3-section">

                <div class="w3-col" style="width:250px;margin-top:10px">
                    <a>Nama Dosen yang diusulkan 	: </a>
                </div>
                <div class="w3-rest" style="margin-bottom:20px;max-width:250px">
                    <input id="namalengkap" class="w3-round-large w3-border w3-input w3-border-black"  type="text" name="namalengkap" required></input>
                </div>

                <div class="w3-col" style="width:250px;margin-top:10px">
                    <a>NIP : </a>
                </div>
                <div class="w3-rest" style="margin-bottom:20px;max-width:250px">
                    <input id="nip" class="w3-round-large w3-border w3-input w3-border-black"  type="text" name="nip" required></input>
                </div>

                <div class="w3-col" style="width:250px;margin-top:10px">
                    <a>Pendidikan terakhir 	: </a>
                </div>
                <div class="w3-rest" style="margin-bottom:20px;max-width:250px">
                    <input id="pendidikan" class="w3-round-large w3-border w3-input w3-border-black"  type="text" name="pendidikan" required></input>
                </div>

                <div class="w3-col" style="width:250px;margin-top:10px">
                    <a>S3 	: </a>
                </div>
                <div class="w3-col" style="margin-bottom:20px;max-width:60px">
                    <input id="s3ya" class="w3-check" type="checkbox" name="s3ya"></input>
                    <label>Ya</label>
                </div>
                <div class="w3-rest" style="margin-bottom:20px;max-width:100px">
                  <input id="s3tidak" class="w3-check" type="checkbox" name="s3tidak"></input>
                  <label>Tidak</label>
                </div>

                <div class="w3-col" style="width:250px;margin-top:10px">
                    <a>Tanggal ijazah 	: </a>
                </div>
                <div class="w3-rest" style="margin-bottom:20px;max-width:250px">
                    <input id="ijazah" class="w3-round-large w3-border w3-input w3-border-black"  type="text" name="ijazah" required></input>
                </div>

                <div class="w3-col" style="width:250px;margin-top:10px">
                    <a>Fakultas 	: </a>
                </div>
                <div class="w3-rest" style="margin-bottom:20px;max-width:250px">
                    <select id="faculty_id" class="w3-white w3-input w3-round-large w3-border w3-border-black" style="margin-bottom:10px" name="faculty_id" required>
                        <option value="" disabled selected>Fakultas</option>
                    </select>
                </div>

                <div class="w3-col" style="width:250px;margin-top:10px">
                    <a>Tanggal penilaian 	: </a>
                </div>
                <div class="w3-rest" style="margin-bottom:20px;max-width:250px">
                    <input id="tanggal" class="w3-round-large w3-border w3-input w3-border-black"  type="text" name="tanggal" required></input>
                </div>

            </div>

        </form>

    </div>

    <div class="w3-container" style="padding:10px;margin:auto;margin-top:20px">

        <div class="c2-ssp-regular">
            <form class="w3-container">

                <table class="w3-table-all c2-ssp-regular c2-background-grey w3-hoverable w3-center w3-border w3-border-black">

                    <tr class="w3-light-grey">
                        <th class="w3-center c2-ssp-regular c2-background-grey w3-text-white w3-border w3-border-black" style="width:10px">No.</th>
                        <th class="c2-ssp-regular c2-background-grey w3-text-white w3-border w3-border-black" style="width:1100px">Syarat kenaikan jabatan regular ke  Guru Besar  (Permendikbud No 92 Tahun 2014, Pasal  10  ayat 1)</th>
                        <th class="w3-center c2-ssp-regular c2-background-grey w3-text-white w3-border w3-border-black" style="width:390px">Hasil penilaian (ya/tidak) memenuhi</th>
                    </tr>

                    <tr class="w3-white">
                        <td class="w3-center w3-border w3-border-black">1a.</td>
                        <td class="w3-border w3-border-black">a. paling singkat 2 (dua) tahun menduduki jabatan Lektor;</td>
                        <td class="w3-border w3-border-black">
                            <div class="w3-col " style="margin-bottom:20px;max-width:60px">
                                <input id="ya1a" name="ya1a" class="w3-check" type="checkbox" required></input>
                                <label>Ya</label>
                            </div>
                            <div class="w3-rest" style="margin-bottom:20px;max-width:100px">
                              <input id="tidak1a" name="tidak1a" class="w3-check" type="checkbox" required></input>
                              <label>Tidak</label>
                            </div>
                        </td>
                    </tr>
                    <tr class="w3-white w3-border w3-border-black">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">
                            <textarea name="catatan1a" style="width:100%;height:150px;padding:12px 20px;box-sizing: border-box;border: 2px solid #ccc;border-radius: 4px;background-color: #f8f8f8;resize: none;" placeholder="catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum"></textarea>
                        </td>
                        <td class="w3-border w3-border-black"></td>
                    </tr>


                    <tr class="w3-white">
                        <td class="w3-center w3-border w3-border-black">1b.</td>
                        <td class="w3-border w3-border-black">b. memiliki paling sedikit 4 (empat) karya ilmiah yang dipublikasikan pada jurnal ilmiah internasional bereputasi sebagai penulis pertama;</td>
                        <td class="w3-border w3-border-black">
                            <div class="w3-col" style="margin-bottom:20px;max-width:60px">
                                <input id="ya1b" name="ya1b" class="w3-check" type="checkbox" required></input>
                                <label>Ya</label>
                            </div>
                            <div class="w3-rest" style="margin-bottom:20px;max-width:100px">
                              <input id="tidak1b" name="tidak1b" class="w3-check" type="checkbox" required></input>
                              <label>Tidak</label>
                            </div>
                        </td>
                    </tr>
                    <tr class="w3-white">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">
                            <textarea name="catatan1b" style="width:100%;height:150px;padding:12px 20px;box-sizing: border-box;border: 2px solid #ccc;border-radius: 4px;background-color: #f8f8f8;resize: none;" placeholder="catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum"></textarea>
                        </td>
                        <td class="w3-border w3-border-black"></td>
                    </tr>

                    <tr class="w3-white">
                        <td class="w3-center w3-border w3-border-black">1c.</td>
                        <td class="w3-border w3-border-black">memiliki pengalaman kerja sebagai dosen tetap paling singkat 10 (sepuluh) tahun;</td>
                        <td class="w3-border w3-border-black">
                            <div class="w3-col" style="margin-bottom:20px;max-width:60px">
                                <input id="ya1c" name="ya1c" class="w3-check" type="checkbox" required></input>
                                <label>Ya</label>
                            </div>
                            <div class="w3-rest" style="margin-bottom:20px;max-width:100px">
                              <input id="tidak1c" name="tidak1c" class="w3-check" type="checkbox" required></input>
                              <label>Tidak</label>
                            </div>
                        </td>
                    </tr>
                    <tr class="w3-white">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">
                            <textarea name="catatan1c" style="width:100%;height:150px;padding:12px 20px;box-sizing: border-box;border: 2px solid #ccc;border-radius: 4px;background-color: #f8f8f8;resize: none;" placeholder="catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum"></textarea>
                        </td>
                        <td class="w3-border w3-border-black"></td>
                    </tr>

                    <tr class="w3-white">
                        <td class="w3-center w3-border w3-border-black">1d.</td>
                        <td class="w3-border w3-border-black">memiliki kualifikasi akademik doktor (S3);</td>
                        <td class="w3-border w3-border-black">
                            <div class="w3-col" style="margin-bottom:20px;max-width:60px">
                                <input id="ya1d" name="ya1d" class="w3-check" type="checkbox" required></input>
                                <label>Ya</label>
                            </div>
                            <div class="w3-rest" style="margin-bottom:20px;max-width:100px">
                              <input id="tidak1d" name="tidak1d" class="w3-check" type="checkbox" required></input>
                              <label>Tidak</label>
                            </div>
                        </td>
                    </tr>
                    <tr class="w3-white">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">
                            <textarea name="catatan1d" style="width:100%;height:150px;padding:12px 20px;box-sizing: border-box;border: 2px solid #ccc;border-radius: 4px;background-color: #f8f8f8;resize: none;" placeholder="catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum"></textarea>
                        </td>
                        <td class="w3-border w3-border-black"></td>
                    </tr>

                    <tr class="w3-white">
                        <td class="w3-center w3-border w3-border-black">1e.</td>
                        <td class="w3-border w3-border-black">paling singkat 3 (tahun) setelah memperoleh ijazah doktor (S3); (ada pengecualian bagi yang mempunyai publikasi di jurnal internasional berputasi sebagai penulis pertama setelah lulus S3);</td>
                        <td class="w3-border w3-border-black">
                            <div class="w3-col" style="margin-bottom:20px;max-width:60px">
                                <input id="ya1e" name="ya1e" class="w3-check" type="checkbox" required></input>
                                <label>Ya</label>
                            </div>
                            <div class="w3-rest" style="margin-bottom:20px;max-width:100px">
                              <input id="tidak1e" name="tidak1e" class="w3-check" type="checkbox" required></input>
                              <label>Tidak</label>
                            </div>
                        </td>
                    </tr>
                    <tr class="w3-white">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">
                            <textarea name="catatan1e" style="width:100%;height:150px;padding:12px 20px;box-sizing: border-box;border: 2px solid #ccc;border-radius: 4px;background-color: #f8f8f8;resize: none;" placeholder="catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum"></textarea>
                        </td>
                        <td class="w3-border w3-border-black"></td>
                    </tr>

                    <tr class="w3-white">
                        <td class="w3-center w3-border w3-border-black">1f.</td>
                        <td class="w3-border w3-border-black">telah memenuhi angka kredit yang dipersyaratkan baik secara kumulatif maupun setiap unsur kegiatan sesuai dengan Lampiran I; (AK pendidikan min 35%, AK Penelitian min 45%, AK PKM maks 10%, Penunjang maks 10%). Periksa juga Lampiran III dan IV, Permenpan no 17 tahun 2013.</td>
                        <td class="w3-border w3-border-black">
                            <div class="w3-col" style="margin-bottom:20px;max-width:60px">
                                <input id="ya1f" name="ya1f" class="w3-check" type="checkbox" required></input>
                                <label>Ya</label>
                            </div>
                            <div class="w3-rest" style="margin-bottom:20px;max-width:100px">
                              <input id="tidak1f" name="tidak1f" class="w3-check" type="checkbox" required></input>
                              <label>Tidak</label>
                            </div>
                        </td>
                    </tr>
                    <tr class="w3-white">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">
                            <textarea name="catatan1f" style="width:100%;height:150px;padding:12px 20px;box-sizing: border-box;border: 2px solid #ccc;border-radius: 4px;background-color: #f8f8f8;resize: none;" placeholder="catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum"></textarea>
                        </td>
                        <td class="w3-border w3-border-black"></td>
                    </tr>

                    <tr class="w3-white">
                        <td class="w3-center w3-border w3-border-black">1g.</td>
                        <td class="w3-border w3-border-black">memiliki kinerja, integritas, etika dan tata krama, serta tanggung jawab</td>
                        <td class="w3-border w3-border-black">
                            <div class="w3-col" style="margin-bottom:20px;max-width:60px">
                                <input id="ya1g" name="ya1g" class="w3-check" type="checkbox" required></input>
                                <label>Ya</label>
                            </div>
                            <div class="w3-rest" style="margin-bottom:20px;max-width:100px">
                              <input id="tidak1g" name="tidak1g" class="w3-check" type="checkbox" required></input>
                              <label>Tidak</label>
                            </div>
                        </td>
                    </tr>
                    <tr class="w3-white">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">
                            <textarea name="catatan1g" style="width:100%;height:150px;padding:12px 20px;box-sizing: border-box;border: 2px solid #ccc;border-radius: 4px;background-color: #f8f8f8;resize: none;" placeholder="catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum"></textarea>
                        </td>
                        <td class="w3-border w3-border-black"></td>
                    </tr>

                    <tr class="w3-white">
                        <td class="w3-center w3-border w3-border-black">2.</td>
                        <td class="w3-border w3-border-black">Memenuhi Matrik Keterkaitan Bidang Ilmu S3, Bidang Ilmu Karya Ilmiah dengan Bidang Ilmu Penugasan Profesor. ((Pedoman Opersional PAK DIKTI)</td>
                        <td class="w3-border w3-border-black">
                            <div class="w3-col" style="margin-bottom:20px;max-width:60px">
                                <input id="ya2" name="ya2" class="w3-check" type="checkbox" required></input>
                                <label>Ya</label>
                            </div>
                            <div class="w3-rest" style="margin-bottom:20px;max-width:100px">
                              <input id="tidak2" name="tidak2" class="w3-check" type="checkbox" required></input>
                              <label>Tidak</label>
                            </div>
                        </td>
                    </tr>
                    <tr class="w3-white">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">
                            <textarea name="catatan2" style="width:100%;height:150px;padding:12px 20px;box-sizing: border-box;border: 2px solid #ccc;border-radius: 4px;background-color: #f8f8f8;resize: none;" placeholder="catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum"></textarea>
                        </td>
                        <td class="w3-border w3-border-black"></td>
                    </tr>

                    <tr class="w3-white">
                        <td class="w3-center w3-border w3-border-black">3.</td>
                        <td class="w3-border w3-border-black">Prosentase karya ilmiah Tabel 6, Pedoman Opersional PAK DIKTI ( Karya Ilmiah dalam a. Jurnal Nasional, b. Prosiding Nasional, c. Poster dan Prosiding Nasional – masing masing maksimum 25% dari AK yg dibutuhkan di penelitian ; Karya Ilmiah dalam Koran/majalah atau tak dipublikasikan maksimum 5% dari AK yang dibutuhkan)</td>
                        <td class="w3-border w3-border-black">
                            <div class="w3-col" style="margin-bottom:20px;max-width:60px">
                                <input id="ya3" name="ya3" class="w3-check" type="checkbox" required></input>
                                <label>Ya</label>
                            </div>
                            <div class="w3-rest" style="margin-bottom:20px;max-width:100px">
                              <input id="tidak3" name="tidak3" class="w3-check" type="checkbox" required></input>
                              <label>Tidak</label>
                            </div>
                        </td>
                    </tr>
                    <tr class="w3-white">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">
                            <textarea name="catatan3" style="width:100%;height:150px;padding:12px 20px;box-sizing: border-box;border: 2px solid #ccc;border-radius: 4px;background-color: #f8f8f8;resize: none;" placeholder="Catatan : jelaskan apakah jumlahkan apakah prosentase jumlah nilai masing masing kategori sesuai ketentuan."></textarea>
                        </td>
                        <td class="w3-border w3-border-black"></td>
                    </tr>

                    <tr class="w3-white">
                        <td class="w3-center w3-border w3-border-black">4.</td>
                        <td class="w3-border w3-border-black">Surat Edaran No 1864 ttd 1 Oktober 2015 Dirjen Sumber Daya IPTEK dan DIKTI, Kementrian RisTek dan Dikti tentang kelengkapan lampiran untuk proses on-line. (DGB memeriksa terutama link karya ilmiah, lampiran 4 ttg Surat Pernyataan Keabsahan Karya Ilmiah, Lampiran – Peer Review Karya Ilmiah dan sertfikat Dosen. Kelengkapan lain biasanya disediakan oleh SDM)</td>
                        <td class="w3-border w3-border-black">
                            <div class="w3-col" style="margin-bottom:20px;max-width:60px">
                                <input id="ya4" name="ya4" class="w3-check" type="checkbox" required></input>
                                <label>Ya</label>
                            </div>
                            <div class="w3-rest" style="margin-bottom:20px;max-width:100px">
                              <input id="tidak4" name="tidak4" class="w3-check" type="checkbox" required></input>
                              <label>Tidak</label>
                            </div>
                        </td>
                    </tr>
                    <tr class="w3-white">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">
                            <textarea name="catatan4" style="width:100%;height:150px;padding:12px 20px;box-sizing: border-box;border: 2px solid #ccc;border-radius: 4px;background-color: #f8f8f8;resize: none;" placeholder="Catatan: tuliskan kelengkapan tersebut disini."></textarea>
                        </td>
                        <td class="w3-border w3-border-black"></td>
                    </tr>

                    <tr class="w3-white w3-border w3-border-black">
                        <td class="w3-border w3-border-black">5.</td>
                        <td class="w3-border w3-border-black">
                            <textarea name="catatan5" style="width:100%;height:150px;padding:12px 20px;box-sizing: border-box;border: 2px solid #ccc;border-radius: 4px;background-color: #f8f8f8;resize: none;" placeholder="Catatan: tuliskan catatan lainnya di sini."></textarea>
                        </td>
                        <td class="w3-border w3-border-black"></td>
                    </tr>

                </table>

                <div class="w3-row w3-section">

                    <div class="w3-col" style="width:77px;margin-top:10px">
                        <a>Penilai 1 	: </a>
                    </div>
                    <div class="w3-rest" style="margin-bottom:20px;max-width:250px">
                        <input id="penilai1" class="w3-round-large w3-border w3-input w3-border-black"  type="text" name="penilai1" required></input>
                    </div>

                    <div class="w3-col" style="width:77px;margin-top:10px">
                        <a>Penilai 2 : </a>
                    </div>
                    <div class="w3-rest" style="margin-bottom:20px;max-width:250px">
                        <input id="penilai2" class="w3-round-large w3-border w3-input w3-border-black"  type="text" name="penilai2" required></input>
                    </div>

                </div>

            </form>
        </div>

    </div>

</div>

@endsection
