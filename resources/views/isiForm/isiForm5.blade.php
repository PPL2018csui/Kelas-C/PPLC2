@extends('layouts.app')

@section('content')

<div class="w3-container" style="max-width:1500px;padding:10px;margin:auto;margin-top:20px">

    <p class="c2-domine w3-center">Komite 5 (promosi dan demosi) Dewan Guru Besar - Universitas Indonesia </p>
    <p class="c2-domine w3-center">Email: dgb-ui-komite5@googlegroups.com</p>
    <br>
    <p class="c2-domine w3-center">Formulir 5: Penilaian usulan kenaikan pangkat dalam Jabatan yang sama</p>
    <p class="c2-domine w3-center">=================================================================================================</p>

    <div class="w3-container" style="margin-left:57px;margin-top:30px">

        <form>

            <div class="w3-row w3-section">

                <div class="w3-col" style="width:250px;margin-top:10px">
                    <a>Nama Dosen yang diusulkan 	: </a>
                </div>
                <div class="w3-rest" style="margin-bottom:20px;max-width:250px">
                    <input id="namalengkap" class="w3-round-large w3-border w3-input w3-border-black"  type="text" name="namalengkap" required></input>
                </div>

                <div class="w3-col" style="width:250px;margin-top:10px">
                    <a>NIP : </a>
                </div>
                <div class="w3-rest" style="margin-bottom:20px;max-width:250px">
                    <input id="nip" class="w3-round-large w3-border w3-input w3-border-black"  type="text" name="nip" required></input>
                </div>

                <div class="w3-col" style="width:250px;margin-top:10px">
                    <a>Pendidikan terakhir 	: </a>
                </div>
                <div class="w3-rest" style="margin-bottom:20px;max-width:250px">
                    <input id="pendidikan" class="w3-round-large w3-border w3-input w3-border-black"  type="text" name="pendidikan" required></input>
                </div>

                <div class="w3-col" style="width:250px;margin-top:10px">
                    <a>Pangkat terakhir 	: </a>
                </div>
                <div class="w3-rest" style="margin-bottom:20px;max-width:250px">
                    <input id="pangkat" class="w3-round-large w3-border w3-input w3-border-black"  type="text" name="pangkat"></input>
                </div>

                <div class="w3-col" style="width:250px;margin-top:10px">
                    <a>Jabatan terakhir 	: </a>
                </div>
                <div class="w3-rest" style="margin-bottom:20px;max-width:250px">
                    <input id="jabatan" class="w3-round-large w3-border w3-input w3-border-black"  type="text" name="jabatan" required></input>
                </div>

                <div class="w3-col" style="width:250px;margin-top:10px">
                    <a>Fakultas 	: </a>
                </div>
                <div class="w3-rest" style="margin-bottom:20px;max-width:250px">
                    <select id="faculty_id" class="w3-white w3-input w3-round-large w3-border w3-border-black" style="margin-bottom:10px" name="faculty_id" required>
                        <option value="" disabled selected>Fakultas</option>
                    </select>
                </div>

                <div class="w3-col" style="width:250px;margin-top:10px">
                    <a>Tanggal penilaian 	: </a>
                </div>
                <div class="w3-rest" style="margin-bottom:20px;max-width:250px">
                    <input id="tanggal" class="w3-round-large w3-border w3-input w3-border-black"  type="text" name="tanggal" required></input>
                </div>

            </div>

        </form>

    </div>

    <div class="w3-container" style="padding:10px;margin:auto;margin-top:20px">

        <div class="c2-ssp-regular">
            <form class="w3-container">

                <table class="w3-table-all c2-ssp-regular c2-background-grey w3-hoverable w3-center w3-border w3-border-black">

                    <tr class="w3-light-grey">
                        <th class="w3-center c2-ssp-regular c2-background-grey w3-text-white w3-border w3-border-black" style="width:10px">No.</th>
                        <th class="c2-ssp-regular c2-background-grey w3-text-white w3-border w3-border-black" style="width:1100px">Syarat kenaikan jabatan regular ke  Guru Besar  (Permendikbud No 92 Tahun 2014, Pasal  10  ayat 1)</th>
                        <th class="w3-center c2-ssp-regular c2-background-grey w3-text-white w3-border w3-border-black" style="width:390px">Hasil penilaian (ya/tidak) memenuhi</th>
                    </tr>

                    <tr class="w3-white">
                        <td class="w3-center w3-border w3-border-black">1.</td>
                        <td class="w3-border w3-border-black">Ayat 1. Kenaikan pangkat dapat dilakukan apabila paling singkat 2 (dua) tahun dalam pangkat terakhir.</td>
                        <td class="w3-border w3-border-black">
                            <input id="lama" class="w3-round-large w3-border w3-input w3-border-black" placeholder="Lama pangkat terakhir (tahun)" type="text" name="lama" required></input>
                        </td>
                    </tr>
                    <tr class="w3-white w3-border w3-border-black">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">
                            <textarea name="catatan1" style="width:100%;height:150px;padding:12px 20px;box-sizing: border-box;border: 2px solid #ccc;border-radius: 4px;background-color: #f8f8f8;resize: none;" placeholder="catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum"></textarea>
                        </td>
                        <td class="w3-border w3-border-black"></td>
                    </tr>


                    <tr class="w3-white">
                        <td class="w3-center w3-border w3-border-black">2.</td>
                        <td class="w3-border w3-border-black">Ayat 2, butir a telah memenuhi angka kredit yang dipersyaratkan baik secara kumulatif maupun setiap unsur kegiatan pada lingkup jabatan tersebut</td>
                        <td class="w3-border w3-border-black">
                            <div class="w3-col" style="margin-bottom:20px;max-width:60px">
                                <input id="ya2" name="ya2" class="w3-check" type="checkbox" required></input>
                                <label>Ya</label>
                            </div>
                            <div class="w3-rest" style="margin-bottom:20px;max-width:100px">
                              <input id="tidak2" name="tidak2" class="w3-check" type="checkbox" required></input>
                              <label>Tidak</label>
                            </div>
                        </td>
                    </tr>
                    <tr class="w3-white">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">
                            <textarea name="catatan2" style="width:100%;height:150px;padding:12px 20px;box-sizing: border-box;border: 2px solid #ccc;border-radius: 4px;background-color: #f8f8f8;resize: none;" placeholder="catatan: tuliskan disini alasan penilaian apakah syarat sudah dipenuhi atau belum"></textarea>
                        </td>
                        <td class="w3-border w3-border-black"><input id="kredit" class="w3-round-large w3-border w3-input w3-border-black" placeholder="Angka kredit yang telah dikumpulkan" type="text" name="kredit" required></input></td>
                    </tr>

                    <tr class="w3-white">
                        <td class="w3-center w3-border w3-border-black">1c.</td>
                        <td class="w3-border w3-border-black">c. memiliki paling sedikit 2 (dua) karya ilmiah yang dipublikasikan pada jurnal ilmiah internasional bereputasi sebagai penulis pertama; dan</td>
                        <td class="w3-border w3-border-black">
                            <div class="w3-col" style="margin-bottom:20px;max-width:60px">
                                <input id="ya1c" name="ya1c" class="w3-check" type="checkbox" required></input>
                                <label>Ya</label>
                            </div>
                            <div class="w3-rest" style="margin-bottom:20px;max-width:100px">
                              <input id="tidak1c" name="tidak1c" class="w3-check" type="checkbox" required></input>
                              <label>Tidak</label>
                            </div>
                        </td>
                    </tr>
                    <tr class="w3-white">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">
                            <textarea name="catatan1c" style="width:100%;height:150px;padding:12px 20px;box-sizing: border-box;border: 2px solid #ccc;border-radius: 4px;background-color: #f8f8f8;resize: none;" placeholder="catatan: jelaskan disini apakah syarat sudah dipenuhi atau belum dengan menuliskan apakah ada judul karya ilmiah yang memenuhi syarat tersebut."></textarea>
                        </td>
                        <td class="w3-border w3-border-black"></td>
                    </tr>

                    <tr class="w3-white">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">
                            <strong class="c2c2-ssp-regular">Bagi Lektor atau Lektor Kepala</strong>
                        </td>
                        <td class="w3-border w3-border-black"></td>
                    </tr>

                    <tr class="w3-white">
                        <td class="w3-center w3-border w3-border-black">3.</td>
                        <td class="w3-border w3-border-black">Ayat 2, buti2 b memiliki karya ilmiah yang dipublikasikan dalam jurnal ilmiah nasional dan/atau internasional untuk jabatan Lektor dan Lektor Kepala sebagai penulis utama; dan</td>
                        <td class="w3-border w3-border-black">
                            <div class="w3-col" style="margin-bottom:20px;max-width:60px">
                                <input id="ya3" name="ya3" class="w3-check" type="checkbox" required></input>
                                <label>Ya</label>
                            </div>
                            <div class="w3-rest" style="margin-bottom:20px;max-width:100px">
                              <input id="tidak3" name="tidak3" class="w3-check" type="checkbox" required></input>
                              <label>Tidak</label>
                            </div>
                        </td>
                    </tr>
                    <tr class="w3-white">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">
                            <textarea name="catatan3" style="width:100%;height:150px;padding:12px 20px;box-sizing: border-box;border: 2px solid #ccc;border-radius: 4px;background-color: #f8f8f8;resize: none;" placeholder="catatan: tuliskan di jurnal apa dan apa judul makalahnya"></textarea>
                        </td>
                        <td class="w3-border w3-border-black"></td>
                    </tr>

                    <tr class="w3-white">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">
                            <strong class="c2c2-ssp-regular">Bagi Guru Besar</strong>
                        </td>
                        <td class="w3-border w3-border-black"></td>
                    </tr>

                    <tr class="w3-white">
                        <td class="w3-center w3-border w3-border-black">4.</td>
                        <td class="w3-border w3-border-black">Ayat 2, butir c memiliki karya ilmiah yang dipublikasikan dalam jurnal ilmiah nasional terakreditasi untuk jabatan Profesor sebagai penulis utama</td>
                        <td class="w3-border w3-border-black"></td>
                    </tr>
                    <tr class="w3-white">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">
                            <textarea name="catatan4" style="width:100%;height:150px;padding:12px 20px;box-sizing: border-box;border: 2px solid #ccc;border-radius: 4px;background-color: #f8f8f8;resize: none;" placeholder="catatan: tuliskan di jurnal apa dan apa judul makalahnya"></textarea>
                        </td>
                        <td class="w3-border w3-border-black"></td>
                    </tr>

                    <tr class="w3-white">
                        <td class="w3-center w3-border w3-border-black">5.</td>
                        <td class="w3-border w3-border-black">Ayat 3 Dosen yang telah memperoleh kenaikan jabatan secara reguler namun pangkatnya masih dalam lingkup jabatan sebelumnya, maka untuk kenaikan pangkat berikutnya tidak disyaratkan tambahan angka kredit sampai pada pangkat maksimum dalam lingkup jabatan tersebut apabila jumlah angka kredit yang telah ditetapkan memenuhi.</td>
                        <td class="w3-border w3-border-black">
                            <div class="w3-col" style="margin-bottom:20px;max-width:60px">
                                <input id="ya5" name="ya5" class="w3-check" type="checkbox" required></input>
                                <label>Ya</label>
                            </div>
                            <div class="w3-rest" style="margin-bottom:20px;max-width:100px">
                              <input id="tidak5" name="tidak5" class="w3-check" type="checkbox" required></input>
                              <label>Tidak</label>
                            </div>
                        </td>
                    </tr>
                    <tr class="w3-white">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">
                            <textarea name="catatan5" style="width:100%;height:150px;padding:12px 20px;box-sizing: border-box;border: 2px solid #ccc;border-radius: 4px;background-color: #f8f8f8;resize: none;" placeholder="Catatan: tuliskan kelengkapan tersebut disini."></textarea>
                        </td>
                        <td class="w3-border w3-border-black"></td>
                    </tr>

                    <tr class="w3-white">
                        <td class="w3-center w3-border w3-border-black">6.</td>
                        <td class="w3-border w3-border-black">Ayat 4 Dosen yang telah memperoleh kenaikan jabatan secara loncat jabatan, maka kenaikan pangkat berikutnya sampai pada pangkat maksimum dalam lingkup jabatan setingkat lebih tinggi dari jabatan semula tidak lagi disyaratkan tambahan angka kredit, sedangkan untuk kenaikan pangkat sampai pada pangkat maksimum dalam lingkup jabatan yang diperoleh melalui loncat jabatan sesuai dengan jumlah angka kredit yang telah ditetapkan, wajib mengumpulkan tambahan angka kredit sebanyak 30% dari unsur utama yang disyaratkan untuk kenaikan pangkat tersebut</td>
                        <td class="w3-border w3-border-black"></td>

                    </tr>
                    <tr class="w3-white">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">Periksa apakah jabatan terakhir karena loncat jabatan, dan periksa apakah syara ini dipenuhi.</td>
                        <td class="w3-border w3-border-black">
                            <div class="w3-col" style="margin-bottom:20px;max-width:60px">
                                <input id="ya6" name="ya6" class="w3-check" type="checkbox" required></input>
                                <label>Ya</label>
                            </div>
                            <div class="w3-rest" style="margin-bottom:20px;max-width:100px">
                              <input id="tidak6" name="tidak6" class="w3-check" type="checkbox" required></input>
                              <label>Tidak</label>
                            </div>
                        </td>
                    </tr>
                    <tr class="w3-white">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">Loncat?</td>
                        <td class="w3-border w3-border-black">
                            <div class="w3-col" style="margin-bottom:20px;max-width:60px">
                                <input id="yal6a" name="ya6a" class="w3-check" type="checkbox" required></input>
                                <label>Ya</label>
                            </div>
                            <div class="w3-rest" style="margin-bottom:20px;max-width:100px">
                              <input id="tidak6a" name="tidak6a" class="w3-check" type="checkbox" required></input>
                              <label>Tidak</label>
                            </div>
                        </td>
                    </tr>
                    <tr class="w3-white">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">Jika loncat jabatan, apakah tambahan angka kredit 30% telah dipenuhi: ya atau tidak</td>
                        <td class="w3-border w3-border-black">
                            <div class="w3-col" style="margin-bottom:20px;max-width:60px">
                                <input id="yal6b" name="ya6b" class="w3-check" type="checkbox" required></input>
                                <label>Ya</label>
                            </div>
                            <div class="w3-rest" style="margin-bottom:20px;max-width:100px">
                              <input id="tidak6b" name="tidak6b" class="w3-check" type="checkbox" required></input>
                              <label>Tidak</label>
                            </div>
                        </td>
                    </tr>

                    <tr class="w3-white">
                        <td class="w3-center w3-border w3-border-black">7.</td>
                        <td class="w3-border w3-border-black">Surat Edaran No 1864 ttd 1 Oktober 2015 Dirjen Sumber Daya IPTEK dan DIKTI, Kementrian RisTek dan Dikti tentang kelengkapan lampiran untuk proses on-line. (DGB memeriksa terutama link karya ilmiah, lampiran 4 ttg Surat Pernyataan Keabsahan Karya Ilmiah, Lampiran – Peer Review Karya Ilmiah ) dan sertfikat Dosen Kelengkapan lain biasanya disediakan oleh SDM</td>
                        <td class="w3-border w3-border-black">
                            <div class="w3-col" style="margin-bottom:20px;max-width:60px">
                                <input id="ya7" name="ya7" class="w3-check" type="checkbox" required></input>
                                <label>Ya</label>
                            </div>
                            <div class="w3-rest" style="margin-bottom:20px;max-width:100px">
                              <input id="tidak7" name="tidak7" class="w3-check" type="checkbox" required></input>
                              <label>Tidak</label>
                            </div>
                        </td>
                    </tr>
                    <tr class="w3-white">
                        <td class="w3-border w3-border-black"></td>
                        <td class="w3-border w3-border-black">
                            <textarea name="catatan7" style="width:100%;height:150px;padding:12px 20px;box-sizing: border-box;border: 2px solid #ccc;border-radius: 4px;background-color: #f8f8f8;resize: none;" placeholder="Catatan: tuliskan kelengkapan tersebut disini."></textarea>
                        </td>
                        <td class="w3-border w3-border-black"></td>
                    </tr>

                    <tr class="w3-white w3-border w3-border-black">
                        <td class="w3-border w3-border-black">8.</td>
                        <td class="w3-border w3-border-black">
                            <textarea name="catatan4" style="width:100%;height:150px;padding:12px 20px;box-sizing: border-box;border: 2px solid #ccc;border-radius: 4px;background-color: #f8f8f8;resize: none;" placeholder="Catatan: tuliskan catatan lainnya di sini."></textarea>
                        </td>
                        <td class="w3-border w3-border-black"></td>
                    </tr>

                </table>

                <div class="w3-row w3-section">

                    <div class="w3-col" style="width:77px;margin-top:10px">
                        <a>Penilai 1 	: </a>
                    </div>
                    <div class="w3-rest" style="margin-bottom:20px;max-width:250px">
                        <input id="penilai1" class="w3-round-large w3-border w3-input w3-border-black"  type="text" name="penilai1" required></input>
                    </div>

                    <div class="w3-col" style="width:77px;margin-top:10px">
                        <a>Penilai 2 : </a>
                    </div>
                    <div class="w3-rest" style="margin-bottom:20px;max-width:250px">
                        <input id="penilai2" class="w3-round-large w3-border w3-input w3-border-black"  type="text" name="penilai2" required></input>
                    </div>

                </div>

            </form>
        </div>

    </div>

</div>

@endsection
