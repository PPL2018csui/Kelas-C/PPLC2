@extends('layouts.app')

@section('content')

<div class="w3-container w3-padding-large">
  @if(session()->has('message'))
      <div id="id01" class="w3-modal" style="display:block">
          <div class="w3-modal-content w3-card-4 w3-green w3-round-large" style="width:300px;height:400px;text-align:center">
              <i class="fas fa-check" style="font-size:150px;"></i>
              <br>
              <h1>SUCCESS</h1>
              <br>
              <p class="w3-padding">{{ session()->get('message') }}</p>
              <button onclick="document.getElementById('id01').style.display='none'"
                class="w3-button w3-margin-bottom w3-hover-black w3-light-grey w3-text-grey w3-hover-opacity w3-border w3-round-large c2-ssp-regular w3-display-bottommiddle"
                style="width:80%">
                Ok
              </button>
          </div>
      </div>
  @endif


    <h2 class="c2-domine w3-center">Kelola Fakultas</h2>
      <a href="/managefaculty/create"><button dusk="tambah" class="w3-button c2-background-grey w3-text-white c2-width-100 w3-border w3-round c2-ssp-regular">Tambah Fakultas Baru</button></a>
      <br>
      <br>
        <table class="w3-table-all w3-hoverable w3-center">

            <tr class="w3-light-grey">
                <th class="w3-center c2-ssp-regular c2-background-grey w3-text-white">Nama Fakultas</th>
                <th class="w3-center c2-ssp-regular c2-background-grey w3-text-white">Edit</th>
                <th class="w3-center c2-ssp-regular c2-background-grey w3-text-white">Delete</th>
            </tr>
            @foreach ($faculties as $faculty)
              <tr class="w3-border w3-round-large">
                  <td><p class="c2-ssp-regular w3-large">{{$faculty->name}}</p></td>
                  <td class="w3-center"><p><a href="/managefaculty/edit/{{$faculty->id}}" class="w3-button w3-hover-yellow c2-background-grey w3-text-white w3-border w3-round-xlarge c2-ssp-regular">Edit</a></p></td>
                  <td class="w3-center"><p>
                    <button onclick="document.getElementById('{{$faculty->id}}').style.display='block'" class="w3-button w3-hover-yellow c2-background-grey w3-text-white w3-border w3-round-xlarge c2-ssp-regular">Delete</button>

                    <div id="{{$faculty->id}}" class="w3-modal w3-animate-opacity">
                        <div class="w3-modal-content w3-card-4">
                            <header class="w3-container c2-background-yellow">

                                <h3>Confirmation</h3>
                            </header>
                            <div class="w3-container">
                                <p>Apakah anda yakin untuk menghapus {{$faculty->name}}?</p>
                            </div>
                            <footer class="w3-container c2-background-yellow w3-padding">
                                <span onclick="document.getElementById('{{$faculty->id}}').style.display='none'"
                                  class="w3-button w3-right c2-background-grey w3-text-white w3-round-xlarge c2-ssp-regular">Cancel</span>
                                  <form action="/managefaculty/delete/{{$faculty->id}}/" method="get">
                                    <button class="w3-button w3-right w3-margin-right c2-background-grey w3-text-white w3-round-xlarge c2-ssp-regular">Delete</button>
                                  </form>
                            </footer>
                        </div>
                    </div>
                  </p></td>
              </tr>
            @endforeach

        </table>

</div>
@endsection
