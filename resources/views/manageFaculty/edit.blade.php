@extends('layouts.app')

@section('content')

<div class="w3-container">
    <div class="w3-center">
        @if (session()->has('error'))
            <span class="w3-text-red">
                <strong>Error! </strong>{{ session()->get('error') }}
            </span>
        @endif
        <div class="c2-box-center w3-border w3-round-large c2-background-grey c2-ssp-regular">
            <div class="w3-border-bottom w3-border-yellow">
                <h4 class="w3-text-white">Edit Fakultas</h4>
            </div>
            <form action="/managefaculty/edit" method="post" style="margin-top:50px;margin-bottom:50px">
                @csrf
                <div class="w3-padding">
                    <input type="text" name="id" value="{{$faculty->id}}" class="w3-hide">
                    <input class="w3-text-white w3-border-bottom c2-background-grey w3-input w3-border-yellow c2-width-100"
                      placeholder="Nama Fakultas" type="text" name="name" value="{{$faculty->name}}" required autofocus>
                </div>
                <div class="w3-padding">
                    <button type="submit" class="w3-padding c2-background-yellow w3-hover-opacity w3-round-large c2-width-150">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
