@extends('layouts.app')

@section('content')

<div class="w3-container">
  <div class="w3-center">
    @if ($errors->any())
      <div id="id01" class="w3-modal" style="display:block">
          <div class="w3-modal-content w3-card-4 w3-red w3-round-large" style="width:300px;height:400px;text-align:center">
              <i class="fas fa-times" style="font-size:150px;"></i>
              <br>
              <h1 class="c2-ssp-regular">ERROR</h1>
              <br>

              <p class="w3-padding">{{$errors->first()}}</p>
              <button onclick="document.getElementById('id01').style.display='none'"
                class="w3-button w3-margin-bottom w3-hover-black w3-light-grey w3-text-grey w3-hover-opacity w3-border w3-round-large c2-ssp-regular w3-display-bottommiddle"
                style="width:80%">
                Ok
              </button>
          </div>
      </div>
    @endif
    <div class="w3-border w3-round-large c2-background-grey c2-ssp-regular" style="max-width:400px;padding:10px;margin:auto;margin-top:90px">
      <div class="w3-border-bottom w3-border-yellow">
          <h4 class="w3-text-white">Tambah Fakultas</h4>
      </div>
      <form action="/managefaculty/create" method="post" style="margin-top:50px;margin-bottom:50px">
        @csrf
        <div class="w3-padding">
          <input class="w3-text-white w3-border-bottom c2-background-grey w3-input w3-border-yellow c2-width-100"
            placeholder="Masukan Nama Fakultas Baru" type="text" name="name" value="" required autofocus>
        </div>
        <div class="w3-padding">
          <button type="submit" class="w3-padding c2-background-yellow w3-hover-opacity w3-round-large c2-width-150">Tambah</button>
        </div>

      </form>
    </div>
  </div>
</div>

@endsection
